<?php 
function feeds_price_field_revision_admin_form($form, &$form_state) {
  $form = array();
  $fields_all = field_info_instances('node');
  $fields_admin = array();
  $fields_image = array();
  $field_vars = variable_get('feeds_price_field_revision_fields', array());

  foreach ($fields_all as $type => $fields) {
    if (is_array($fields) && $fields) {
      foreach ($fields as $field) {
        if (isset($field['widget']['type']) && $field['widget']['type'] ==  'number') {
          $fields_admin[$type][$field['field_name']] = $field['label'];
        }
        if (isset($field['widget']['type']) && $field['widget']['type'] ==  'image_image') {
          $fields_image[$type][$field['field_name']] = $field['label'];
        }
      }
    }
  }
  
  if (!$fields_admin) {
    drupal_set_message(t('Please add at least 2 Decimal number fields to a node type to enable this feature.'), 'warning');
    return $form;
  }
  if (!$fields_image) {
    drupal_set_message(t('Please add an image field if you want to update the node based on the new price field\'s value'), 'warning');
  }
  
  $found = FALSE;
  foreach ($fields_admin as $node_type => $fields_list) {
    if (count($fields_list) >= 2) {
      $found = TRUE;
      break;
    }
  }
  if ($found === FALSE) {
    drupal_set_message(t('Please add at least 2 Decimal number fields to a node type to enable this feature. At least one content type exists with a Number field.'), 'warning');
    return;
  }

  $form['feeds_price_field_revision_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#options' => array('disabled' => t('- Disabled -')) + drupal_map_assoc(array_keys($fields_admin)),
    '#default_value' => variable_get('feeds_price_field_revision_type', 'disabled'),
    '#description' => t('Choose the node type that you want to add the field archive effect to. Only node types with at least 2 decimal fields are shown here. '),
    '#ajax' => array(
      'callback' => 'feeds_price_field_revision_admin_form_ajax_callback',
      'wrapper' => 'replace_textfield_div',
      'effect' => 'slide',
      'speed' => 'slow',
    ),
  );
  $form['feeds_price_field_revision_fields'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#title' => t('Fields'),
    '#prefix' => '<div id="replace_textfield_div">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  $fields = array('new' => 'Your Price', 'old' => 'Your Old Price');
  $field_opts = array();
  if (isset($form_state['values']['feeds_price_field_revision_type'])) {
    $ajax_node_type = $form_state['values']['feeds_price_field_revision_type'];
  } else {
    $ajax_node_type = variable_get('feeds_price_field_revision_type', 'disabled');
  }
    if ($ajax_node_type == 'disabled') {
      $form['feeds_price_field_revision_fields']['#type'] = 'markup';
      $form['feeds_price_field_revision_fields']['#markup'] = '<div id="replace_textfield_div"></div>';
    }
    elseif (isset($fields_admin[$ajax_node_type])) {
      $field_opts = $fields_admin[$ajax_node_type];
    }

  foreach ($fields as $field_m => $field_l) {
    $form['feeds_price_field_revision_fields'][$field_m] = array(
    '#type' => 'select',
    '#title' => t($field_l),
    '#description' => t('Choose the field for the %new field', array('%new' => t($field_l))),
    '#options' => $field_opts,
    '#default_value' => isset($field_vars[$field_m]) ? $field_vars[$field_m] : '',
    );
  }
  $form['feeds_price_field_revision_fields']['image'] = array(
    '#type' => 'select',
    '#title' => t('Image field'),
    '#description' => t('Choose the field for the %new field', array('%new' => t('Price difference image'))),
    '#options' => isset($fields_image[$ajax_node_type]) ? array('' => t('- Disabled -')) + $fields_image[$ajax_node_type] : array(),
    '#default_value' => isset($field_vars['image']) ? $field_vars['image'] : '',
  );

  
  
  
  $form['feeds_price_field_revision_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field change images'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
  );
  
  $files = variable_get('feeds_price_field_revision_files', array());
  $form_state['existing_files'] = $files;

  $actions = array('up' => t('Price increase'), 'down' => t('Price decrease'));
  foreach ($actions as $action => $label) {
    if (isset($files[$action]) && $image = file_load($files[$action])) {
      $form['feeds_price_field_revision_files'][$action.'_preview'] = array(
        '#type' => 'item',
        '#title' => t('Current image preview: %action', array('%action' => $label)),
        '#markup' => theme('image', array(
          'path' => file_create_url($image->uri),
          'alt' => t('Current image preview: %action', array('%action' => $label)),
          'title' => t('Current image preview: %action', array('%action' => $label)),
        )),
      );
    }
    $form['feeds_price_field_revision_files'][$action] = array(
      '#type' => 'managed_file',
      '#title' => t('%action image', array('%action' => $label)),
      '#description' => t('Allowed extensions: gif png jpg jpeg'),
      '#default_value' => isset($files[$action]) ? $files[$action] : '',
      '#upload_location' => 'public://',
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );
    $form_state['feeds_price_field_revision_current'][$action] = isset($files[$action]) ? $files[$action] : NULL;
  }

  $form = system_settings_form($form);
  $form['#submit'][] = 'feeds_price_field_revision_admin_form_submit_extra';
  $form['#validate'][] = 'feeds_price_field_revision_admin_form_validate_extra';
  return $form;

}

function feeds_price_field_revision_admin_form_validate_extra($form, &$form_state) {
  if (isset($form_state['values']['feeds_price_field_revision_fields']['new'], $form_state['values']['feeds_price_field_revision_fields']['old']) 
    && $form_state['values']['feeds_price_field_revision_fields']['new'] == $form_state['values']['feeds_price_field_revision_fields']['old']) {
    form_set_error('feeds_price_field_revision_fields][old', t('You can\'t use the same field for both new and old values'));
  }
}

function feeds_price_field_revision_admin_form_ajax_callback($form, $form_state) {
  return $form['feeds_price_field_revision_fields'];
}

function feeds_price_field_revision_admin_form_submit_extra($form, &$form_state) {
  $actions = array('up' => t('Up'), 'down' => t('Down'));
  foreach ($actions as $action => $label) {
    $iter = 1;
    if (isset($form_state['values']['feeds_price_field_revision_files'][$action], $form_state['feeds_price_field_revision_current'][$action])) {
       if ($form_state['values']['feeds_price_field_revision_files'][$action] != $form_state['feeds_price_field_revision_current'][$action]) {
        $file = file_load($form_state['values']['feeds_price_field_revision_files'][$action]);
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        file_usage_add($file, 'feeds_price_field_revision', 'node', $iter); 
      }
    }
    $iter++;
  }
}