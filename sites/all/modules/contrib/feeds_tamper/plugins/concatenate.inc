<?php

/**
 * @file
 * Concatenate value with string.
 */

$plugin = array(
  'form' => 'feeds_tamper_concatenate_form',
  'callback' => 'feeds_tamper_concatenate_callback',
  'name' => 'Concatenate string',
  'multi' => 'loop',
  'category' => 'Text',
);

function feeds_tamper_concatenate_form($importer, $element_key, $settings) {
  $form = array();
  $form['cat_str'] = array(
    '#type' => 'textfield',
    '#title' => t('String value to concatenate with'),
    '#default_value' => isset($settings['cat_str']) ? $settings['cat_str'] : '',
  );
  $form['cat_dir'] = array(
    '#type' => 'radios',
    '#title' => t('side to concatenate string to'),
    '#options' => array('CAT_DIR_LEFT' => t('Left'), 'CAT_DIR_RIGHT' => t('Right')),
    '#default_value' => isset($settings['cat_dir']) ? $settings['cat_dir'] : 'CAT_DIR_RIGHT',
  );
  return $form;
}

function feeds_tamper_concatenate_callback($result, $item_key, $element_key, &$field, $settings) {
  if ($settings['cat_dir'] == 'CAT_DIR_RIGHT') {
  	$field = $field . $settings['cat_str'];
  }
  else {
  	$field = $settings['cat_str'] . $field;
  }
}