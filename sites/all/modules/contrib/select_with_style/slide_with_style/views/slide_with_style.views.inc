<?php

/**
 * @file
 * slide_with_style.views.inc
 *
 * Alterations to Views filter handlers.
 */

/**
 * Implements hook_field_views_data_alter().
 *
 * Temporarily swaps out exposed views_handler_filter_numeric filter for a
 * slider version. This function is called for every field on the system, but
 * only the fields required for the View's exposed filters will be converted.
 * @see slide_with_style_handler_filter_numeric
 */
function slide_with_style_field_views_data_alter(&$data, $field) {

  foreach ($data as $table_name => $table_data) {
    foreach ($table_data as $field_name => $field_data) {
      if (isset($field_data['filter']['handler']) && $field_data['filter']['handler'] == 'views_handler_filter_numeric') {
        $data[$table_name][$field_name]['filter']['handler'] = 'slide_with_style_handler_filter_numeric';
      }
    }
  }
}
