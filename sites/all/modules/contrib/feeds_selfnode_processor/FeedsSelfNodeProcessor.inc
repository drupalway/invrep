<?php

/**
 * @file
 * Class definition of FeedsSelfNodeProcessor.
 */

/**
 * Allows a feed node to populate fields on itself.
 */
class FeedsSelfNodeProcessor extends FeedsNodeProcessor {

  /**
   * Process the result of the parsing stage.
   *
   * @param FeedsSource $source
   *   Source information about this import.
   * @param FeedsParserResult $parser_result
   *   The result of the parsing stage.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {

    // We are updating self so we only have one item, no need for while loop.
    $item = $parser_result->shiftItem();

    // We know node exists because it is the source of this import.
    $entity_id = $source->feed_nid;
    $skip_existing = $this->config['update_existing'] == FEEDS_SKIP_EXISTING;

    // If we are not updating return.
    if ($skip_existing) {
      return;
    }

    $hash = $this->hash($item);
    $changed = ($hash !== $this->getHash($entity_id));
    $force_update = $this->config['skip_hash_check'];

    // Do not proceed if the item has not changed, and we're not forcing
    // the update.
    if (!$changed && !$force_update) {
      return;
    }

    try {
      // Load entity.
      $entity = $this->entityLoad($source, $entity_id);
      // The feeds_item table is always updated with the info for the most recently processed entity.
      // The only carryover is the entity_id.
      $this->newItemInfo($entity, $source->feed_nid, $hash);
      $entity->feeds_item->entity_id = $entity_id;

      // Set property and field values.
      $this->map($source, $parser_result, $entity);
      $this->entityValidate($entity);

      // Allow modules to alter the entity before saving.
      module_invoke_all('feeds_presave', $source, $entity, $item);
      if (module_exists('rules')) {
        rules_invoke_event('feeds_import_'. $source->importer()->id, $entity);
      }

      // Enable modules to skip saving at all.
      if (!empty($entity->feeds_item->skip)) {
        return;
      }

      // This will throw an exception on failure.
      $this->entitySaveAccess($entity);
      $this->entitySave($entity);

      // Track progress.
      $state->updated++;
    }
    // Something bad happened, log it.
    catch (Exception $e) {
      $state->failed++;
      drupal_set_message($e->getMessage(), 'warning');
      $message = $e->getMessage();
      $message .= '<h3>Original item</h3>';
      $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
      $message .= '<h3>Entity</h3>';
      $message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
      $source->log('import', $message, array(), WATCHDOG_ERROR);
    }

    // Set messages
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
    );
    $messages = array();
    if ($state->updated) {
      $messages[] = array(
       'message' => t(
         'Updated @number @entity.',
          array('@number' => $state->updated) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => t(
          'Failed importing @number @entity.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $defaults = parent::configDefaults();
    $defaults['update_existing'] = FEEDS_UPDATE_EXISTING;
    $defaults['expire'] = FEEDS_EXPIRE_NEVER;
    return $defaults;
  }

  /**
   * Override parent::configForm().
   */
/*  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    unset($form['content_type']);

    return $form;
  }
*/
  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    // Force content type to be self.
    $this->config['content_type'] = feeds_importer($this->id)->config['content_type'];
    if (empty($this->config['content_type'])) {
      drupal_set_message(t('This importer must be attached to a content type for Feeds Self Node Processor to work.'), 'warning');
      return array('' => array());
    }
    else {
      return parent::getMappingTargets();
    }
  }
}