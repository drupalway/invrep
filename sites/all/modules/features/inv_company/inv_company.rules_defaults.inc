<?php
/**
 * @file
 * inv_company.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function inv_company_default_rules_configuration() {
  $items = array();
  $items['rules_og_new_content_notification_on_update_swf_cloned'] = entity_import('rules_config', '{ "rules_og_new_content_notification_on_update_swf_cloned" : {
      "LABEL" : "OG new content notification on update (SWF - cloned)",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "og", "rules", "inv_company", "php" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "og_entity_is_group_content" : { "entity" : [ "node" ] } },
        { "node_is_published" : { "node" : [ "node" ] } },
        { "data_is" : { "data" : [ "node-unchanged:status" ], "value" : "0" } }
      ],
      "DO" : [
        { "inv_og_get_members" : {
            "USING" : { "group_content" : [ "node" ] },
            "PROVIDE" : { "inv_group_members" : { "inv_group_members" : "List of group members" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "inv-group-members" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "mail" : {
                  "to" : [ "list-item:mail" ],
                  "subject" : "[site:og-context--node] - new update",
                  "message" : "[node:author] has just added a new update called \\u0022[node:title]\\u0022\\r\\n\\r\\nYou can view the update here: \\u003C?php \\r\\n  $og_group_ref = field_get_items(\\u0027node\\u0027, $node, \\u0027og_group_ref\\u0027);\\r\\n  if (!empty($og_group_ref[0][\\u0027target_id\\u0027])) {\\r\\n    $url = drupal_get_path_alias(\\u0027node\\/\\u0027 . $og_group_ref[0][\\u0027target_id\\u0027]);\\r\\n  }\\r\\n  else {\\r\\n    $url = drupal_get_path_alias(\\u0027node\\/\\u0027 . $node-\\u003Enid);\\r\\n  }\\r\\n  $link = url($url, array(\\u0027absolute\\u0027 =\\u003E TRUE));\\r\\n  print $link;\\r\\n?\\u003E \\r\\n\\r\\nEnjoy,\\r\\n - Team Invrep (on behalf of [node:author])\\r\\n\\r\\nPS: Please add invrep@invrep.co to your address book or safe sender list so these emails get to your inbox\\r\\n\\r\\nTo unsubscribe: find [site:og-context--node] on http:\\/\\/www.invrep.co\\/groups and click \\u0022unfollow\\u0022",
                  "from" : [ "" ],
                  "language" : [ "" ]
                }
              }
            ]
          }
        },
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Your update",
            "message" : "An email has been sent to your followers to notify them know you\\u0027ve created a new update called \\u0022[node:title]\\u0022 \\r\\n\\r\\nYou can manage who\\u0027s receiving these notifications from your Company dashboard.\\r\\n\\r\\nThanks,\\r\\n - Team Invrep",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_og_new_content_notification_swf_cloned_'] = entity_import('rules_config', '{ "rules_og_new_content_notification_swf_cloned_" : {
      "LABEL" : "OG new content notification (SWF - cloned)",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "og", "rules", "inv_company", "php" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "og_entity_is_group_content" : { "entity" : [ "node" ] } },
        { "node_is_published" : { "node" : [ "node" ] } }
      ],
      "DO" : [
        { "inv_og_get_members" : {
            "USING" : { "group_content" : [ "node" ] },
            "PROVIDE" : { "inv_group_members" : { "inv_group_members" : "List of group members" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "inv-group-members" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "mail" : {
                  "to" : [ "list-item:mail" ],
                  "subject" : "[site:og-context--node] - new update",
                  "message" : "[node:author] has just added a new update called \\u0022[node:title]\\u0022\\r\\n\\r\\nYou can view the update here: \\u003C?php \\r\\n  $og_group_ref = field_get_items(\\u0027node\\u0027, $node, \\u0027og_group_ref\\u0027);\\r\\n  if (!empty($og_group_ref[0][\\u0027target_id\\u0027])) {\\r\\n    $url = drupal_get_path_alias(\\u0027node\\/\\u0027 . $og_group_ref[0][\\u0027target_id\\u0027]);\\r\\n  }\\r\\n  else {\\r\\n    $url = drupal_get_path_alias(\\u0027node\\/\\u0027 . $node-\\u003Enid);\\r\\n  }\\r\\n  $link = url($url, array(\\u0027absolute\\u0027 =\\u003E TRUE));\\r\\n  print $link;\\r\\n?\\u003E \\r\\n\\r\\nEnjoy,\\r\\n - Team Invrep (on behalf of [node:author])\\r\\n\\r\\nPS: Please add invrep@invrep.co to your address book or safe sender list so these emails get to your inbox\\r\\n\\r\\nTo unsubscribe: find [site:og-context--node] on http:\\/\\/www.invrep.co\\/groups and click \\u0022unfollow\\u0022",
                  "from" : [ "" ],
                  "language" : [ "" ]
                }
              }
            ]
          }
        },
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Your update",
            "message" : "An email has been sent to your followers to notify them know you\\u0027ve created a new update called \\u0022[node:title]\\u0022 \\r\\n\\r\\nYou can manage who\\u0027s receiving these notifications from your Company dashboard.\\r\\n\\r\\nThanks,\\r\\n - Team Invrep",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
