<?php
/**
 * @file
 * inv_company.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function inv_company_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function inv_company_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function inv_company_eck_bundle_info() {
  $items = array(
  'invitation_invitation' => array(
  'machine_name' => 'invitation_invitation',
  'entity_type' => 'invitation',
  'name' => 'invitation',
  'label' => 'invitation',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function inv_company_eck_entity_type_info() {
$items = array(
       'invitation' => array(
  'name' => 'invitation',
  'label' => 'invitation',
  'properties' => array(
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
  ),
),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function inv_company_node_info() {
  $items = array(
    'flash_update' => array(
      'name' => t('Flash Update'),
      'base' => 'node_content',
      'description' => t('For providing a short, intra-quarter update to investors.

Guidance: flash updates should be used for infrequent, material updates.  They can be used to report hitting a major milestone or activity (e.g. new C-level hire) - or a key minute from a Board or Management meeting.
Flash updates shouldn\'t be sent more than once a week.  And shouldn\'t be a repetition of previous regular reports.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'report' => array(
      'name' => t('Regular Report'),
      'base' => 'node_content',
      'description' => t('For creating and displaying regular reports'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'startup_groups' => array(
      'name' => t('Company'),
      'base' => 'node_content',
      'description' => t('Organic Group for each Startup or SME'),
      'has_title' => '1',
      'title_label' => t('Company name'),
      'help' => '',
    ),
  );
  return $items;
}
