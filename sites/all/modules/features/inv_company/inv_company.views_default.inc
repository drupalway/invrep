<?php
/**
 * @file
 * inv_company.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function inv_company_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'inv_content_drafts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Content drafts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Drafts';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view own unpublished content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<p class="views-label">Your draft Reports</p>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'node/[nid]/edit';
  $handler->display->display_options['fields']['title']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '25';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '0';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'flash_update' => 'flash_update',
    'report' => 'report',
  );
  /* Filter criterion: User: Current */
  $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['table'] = 'users';
  $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid_current']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Your draft Reports';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '0';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'report' => 'report',
  );
  /* Filter criterion: User: Current */
  $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['table'] = 'users';
  $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid_current']['value'] = '1';
  $handler->display->display_options['block_description'] = 'Drafts block';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Your draft Updates';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<p class="views-label">Your draft Updates</p>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'node/[nid]/edit';
  $handler->display->display_options['fields']['title']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '25';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '0';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'flash_update' => 'flash_update',
  );
  /* Filter criterion: User: Current */
  $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['table'] = 'users';
  $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid_current']['value'] = '1';
  $handler->display->display_options['block_description'] = 'Drafts block';
  $export['inv_content_drafts'] = $view;

  $view = new view();
  $view->name = 'quarterly_update_kpis';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Regular Reports KPIs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'KPIs';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    2 => '2',
    4 => '4',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '15';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'chart';
  $handler->display->display_options['style_options']['type'] = 'line';
  $handler->display->display_options['style_options']['library'] = 'highcharts';
  $handler->display->display_options['style_options']['label_field'] = 'field_quarter_end_date';
  $handler->display->display_options['style_options']['data_fields'] = array(
    'field_fc_kpi_actual' => 'field_fc_kpi_actual',
    'field_fc_kpi_budget' => 'field_fc_kpi_budget',
    'item_id' => 0,
    'field_quarter_end_date' => 0,
    'field_fc_kpi' => 0,
  );
  $handler->display->display_options['style_options']['field_colors'] = array(
    'item_id' => '#2f7ed8',
    'field_fc_kpi_actual' => '#0d233a',
    'field_fc_kpi_budget' => '#8bbc21',
    'field_quarter_end_date' => '#910000',
    'field_fc_kpi' => '#1aadce',
  );
  $handler->display->display_options['style_options']['legend_position'] = 'bottom';
  $handler->display->display_options['style_options']['width'] = '';
  $handler->display->display_options['style_options']['height'] = '';
  $handler->display->display_options['style_options']['xaxis_labels_rotation'] = '0';
  $handler->display->display_options['style_options']['yaxis_labels_rotation'] = '0';
  /* Relationship: Field collection item: Entity with the KPIs (field_fc_kpis) */
  $handler->display->display_options['relationships']['field_fc_kpis_node']['id'] = 'field_fc_kpis_node';
  $handler->display->display_options['relationships']['field_fc_kpis_node']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_fc_kpis_node']['field'] = 'field_fc_kpis_node';
  $handler->display->display_options['relationships']['field_fc_kpis_node']['required'] = TRUE;
  /* Relationship: OG membership: OG membership from Content */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['relationship'] = 'field_fc_kpis_node';
  $handler->display->display_options['relationships']['og_membership_rel']['required'] = TRUE;
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  /* Field: Field collection item: Actual */
  $handler->display->display_options['fields']['field_fc_kpi_actual']['id'] = 'field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['table'] = 'field_data_field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['field'] = 'field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Field collection item: Budget */
  $handler->display->display_options['fields']['field_fc_kpi_budget']['id'] = 'field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['table'] = 'field_data_field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['field'] = 'field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Period End Date */
  $handler->display->display_options['fields']['field_quarter_end_date']['id'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['field'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['relationship'] = 'field_fc_kpis_node';
  $handler->display->display_options['fields']['field_quarter_end_date']['settings'] = array(
    'format_type' => 'short_date_display',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Field collection item: KPI */
  $handler->display->display_options['fields']['field_fc_kpi']['id'] = 'field_fc_kpi';
  $handler->display->display_options['fields']['field_fc_kpi']['table'] = 'field_data_field_fc_kpi';
  $handler->display->display_options['fields']['field_fc_kpi']['field'] = 'field_fc_kpi';
  $handler->display->display_options['fields']['field_fc_kpi']['type'] = 'taxonomy_term_reference_plain';
  /* Sort criterion: Content: Period End Date (field_quarter_end_date) */
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['id'] = 'field_quarter_end_date_value';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['field'] = 'field_quarter_end_date_value';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['relationship'] = 'field_fc_kpis_node';
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['relationship'] = 'field_fc_kpis_node';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Field collection item: KPI (field_fc_kpi) (Limited) */
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['id'] = 'field_fc_kpi_tid_limited';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['table'] = 'field_data_field_fc_kpi';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['field'] = 'field_fc_kpi_tid_limited';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['value'] = array(
    0 => '0',
  );
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['operator_id'] = 'field_fc_kpi_tid_limited_op';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['label'] = 'KPI';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['operator'] = 'field_fc_kpi_tid_limited_op';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['identifier'] = 'field_fc_kpi_tid_limited';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );

  /* Display: Block - KPIs */
  $handler = $view->new_display('block', 'Block - KPIs', 'block_kpis');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: View area */
  $handler->display->display_options['header']['view']['id'] = 'view';
  $handler->display->display_options['header']['view']['table'] = 'views';
  $handler->display->display_options['header']['view']['field'] = 'view';
  $handler->display->display_options['header']['view']['view_to_insert'] = 'cash_tab_for_company_dashboard:block_3';
  $handler->display->display_options['header']['view']['inherit_arguments'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  /* Field: Field collection item: Actual */
  $handler->display->display_options['fields']['field_fc_kpi_actual']['id'] = 'field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['table'] = 'field_data_field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['field'] = 'field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Field collection item: Budget */
  $handler->display->display_options['fields']['field_fc_kpi_budget']['id'] = 'field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['table'] = 'field_data_field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['field'] = 'field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Period End Date */
  $handler->display->display_options['fields']['field_quarter_end_date']['id'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['field'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['relationship'] = 'field_fc_kpis_node';
  $handler->display->display_options['fields']['field_quarter_end_date']['settings'] = array(
    'format_type' => 'short_date_display',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Field collection item: KPI */
  $handler->display->display_options['fields']['field_fc_kpi']['id'] = 'field_fc_kpi';
  $handler->display->display_options['fields']['field_fc_kpi']['table'] = 'field_data_field_fc_kpi';
  $handler->display->display_options['fields']['field_fc_kpi']['field'] = 'field_fc_kpi';
  $handler->display->display_options['fields']['field_fc_kpi']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Field collection item: KPI (field_fc_kpi) (Limited) */
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['id'] = 'field_fc_kpi_tid_limited';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['table'] = 'field_data_field_fc_kpi';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['field'] = 'field_fc_kpi_tid_limited';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['value'] = array(
    0 => '0',
  );
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['operator_id'] = 'field_fc_kpi_tid_limited_op';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['label'] = 'Select KPI:';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['operator'] = 'field_fc_kpi_tid_limited_op';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['identifier'] = 'field_fc_kpi_tid_limited';
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['field_fc_kpi_tid_limited']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'field_fc_kpis_node';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Block - Financial */
  $handler = $view->new_display('block', 'Block - Financial', 'block_financial');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Financial Metrics';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'chart';
  $handler->display->display_options['style_options']['type'] = 'line';
  $handler->display->display_options['style_options']['library'] = 'highcharts';
  $handler->display->display_options['style_options']['label_field'] = 'field_quarter_end_date';
  $handler->display->display_options['style_options']['data_fields'] = array(
    'field_fc_kpi_actual' => 'field_fc_kpi_actual',
    'field_fc_kpi_budget' => 'field_fc_kpi_budget',
    'item_id' => 0,
    'field_quarter_end_date' => 0,
    'field_fc_financial_metric' => 0,
  );
  $handler->display->display_options['style_options']['field_colors'] = array(
    'item_id' => '#2f7ed8',
    'field_quarter_end_date' => '#910000',
    'field_fc_kpi_actual' => '#0d233a',
    'field_fc_kpi_budget' => '#8bbc21',
    'field_fc_financial_metric' => '#1aadce',
  );
  $handler->display->display_options['style_options']['legend_position'] = 'bottom';
  $handler->display->display_options['style_options']['width'] = '';
  $handler->display->display_options['style_options']['height'] = '';
  $handler->display->display_options['style_options']['xaxis_labels_rotation'] = '0';
  $handler->display->display_options['style_options']['yaxis_labels_rotation'] = '0';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Field collection item: Entity with the Financial Measure (field_fc_financial_kpis) */
  $handler->display->display_options['relationships']['field_fc_financial_kpis_node']['id'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['relationships']['field_fc_financial_kpis_node']['table'] = 'field_collection_item';
  $handler->display->display_options['relationships']['field_fc_financial_kpis_node']['field'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['relationships']['field_fc_financial_kpis_node']['required'] = TRUE;
  /* Relationship: OG membership: OG membership from Content */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['relationship'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['relationships']['og_membership_rel']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  /* Field: Content: Period End Date */
  $handler->display->display_options['fields']['field_quarter_end_date']['id'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['field'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['relationship'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['fields']['field_quarter_end_date']['settings'] = array(
    'format_type' => 'short_date_display',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Field collection item: Actual */
  $handler->display->display_options['fields']['field_fc_kpi_actual']['id'] = 'field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['table'] = 'field_data_field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['field'] = 'field_fc_kpi_actual';
  $handler->display->display_options['fields']['field_fc_kpi_actual']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_fc_kpi_actual']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Field collection item: Budget */
  $handler->display->display_options['fields']['field_fc_kpi_budget']['id'] = 'field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['table'] = 'field_data_field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['field'] = 'field_fc_kpi_budget';
  $handler->display->display_options['fields']['field_fc_kpi_budget']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Field collection item: Financial Measure */
  $handler->display->display_options['fields']['field_fc_financial_metric']['id'] = 'field_fc_financial_metric';
  $handler->display->display_options['fields']['field_fc_financial_metric']['table'] = 'field_data_field_fc_financial_metric';
  $handler->display->display_options['fields']['field_fc_financial_metric']['field'] = 'field_fc_financial_metric';
  $handler->display->display_options['fields']['field_fc_financial_metric']['label'] = 'Financial metric';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Period End Date (field_quarter_end_date) */
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['id'] = 'field_quarter_end_date_value';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['field'] = 'field_quarter_end_date_value';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['relationship'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['relationship'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Field collection item: Financial Measure (field_fc_financial_metric) (Limited) */
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['id'] = 'field_fc_financial_metric_tid_limited';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['table'] = 'field_data_field_fc_financial_metric';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['field'] = 'field_fc_financial_metric_tid_limited';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['value'] = array(
    0 => '0',
  );
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['expose']['operator_id'] = 'field_fc_financial_metric_tid_limited_op';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['expose']['label'] = 'Financial metric';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['expose']['operator'] = 'field_fc_financial_metric_tid_limited_op';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['expose']['identifier'] = 'field_fc_financial_metric_tid_limited';
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['field_fc_financial_metric_tid_limited']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'field_fc_financial_kpis_node';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $export['quarterly_update_kpis'] = $view;

  return $export;
}
