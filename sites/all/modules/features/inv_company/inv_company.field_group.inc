<?php
/**
 * @file
 * inv_company.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function inv_company_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_financials_group|node|report|form';
  $field_group->group_name = 'group_financials_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'report';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Financials',
    'weight' => '1',
    'children' => array(
      0 => 'field_financials_and_cash_qtr',
      1 => 'field_monthly_revenue',
      2 => 'field_position_cash_in_bank',
      3 => 'field_monthly_cash_burn',
      4 => 'field_budget_monthly_cash_burn',
      5 => 'field_budget_traction_monthly_re',
      6 => 'field_budget_position_cash_in_ba',
      7 => 'field_financials_file_upload',
      8 => 'field_kpi_gross_margin',
      9 => 'field_budget_kpi_gross_margin_pe',
      10 => 'field_financial_and_cash_quick_i',
      11 => 'field_ebit_margin_percentage',
      12 => 'field_budget_ebit_margin_percent',
      13 => 'field_fc_financial_kpis',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_financials_group|node|report|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_kpi_group|node|report|form';
  $field_group->group_name = 'group_kpi_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'report';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'KPIs',
    'weight' => '2',
    'children' => array(
      0 => 'field_traction_active_users',
      1 => 'field_budget_traction_active_use',
      2 => 'field_budget_kpi_cac',
      3 => 'field_kpi_long_tem_value_ltv_',
      4 => 'field_budget_kpi_ltv',
      5 => 'field_employees',
      6 => 'field_budget_employees',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'KPIs',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_kpi_group|node|report|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_kpis|node|report|form';
  $field_group->group_name = 'group_kpis';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'report';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'KPIs',
    'weight' => '3',
    'children' => array(
      0 => 'field_kpis_qtr',
      1 => 'field_kpis_file_upload',
      2 => 'field_kpis_quick_indicator',
      3 => 'field_fc_kpis',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_kpis|node|report|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_overview|node|report|form';
  $field_group->group_name = 'group_overview';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'report';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Overview',
    'weight' => '0',
    'children' => array(
      0 => 'og_group_ref',
      1 => 'field_team_qtr',
      2 => 'field_product_qtr',
      3 => 'field_customer_qtr',
      4 => 'field_market_qtr',
      5 => 'field_opportunities_and_risks_qt',
      6 => 'field_plan_for_next_quarter_qtr',
      7 => 'field_quarter_end_date',
      8 => 'field_quarterly_report',
      9 => 'field_summary',
      10 => 'field_summary_quick',
      11 => 'field_team_quick',
      12 => 'field_product_quick_indicator',
      13 => 'field_customers_quick_indicator',
      14 => 'field_market_quick_indicator',
      15 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Overview',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_overview|node|report|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reminders|node|startup_groups|form';
  $field_group->group_name = 'group_reminders';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'startup_groups';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reminders',
    'weight' => '3',
    'children' => array(
      0 => 'field_how_often_do_you_report',
      1 => 'field_when_is_your_next_report',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_reminders|node|startup_groups|form'] = $field_group;

  return $export;
}
