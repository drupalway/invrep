<?php
/**
 * @file
 * inv_company.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function inv_company_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Companies',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0b477aef-53e1-42ee-9cf8-d5ca806af378',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'New Users',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0c54bc36-3d43-4668-b5cb-f50fe4273f16',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Registered Users',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0ca0b154-2ab4-46db-b949-2c3f12d2312b',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Beta Signups',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '112384c9-f9c0-40dd-b48c-927af040621b',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Advertisers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '12918ae7-8a9e-48c0-adfe-db04d4f1dbc3',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Arpu',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '13016ffb-5a6c-4688-8d9d-0aaa01215bfe',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Upsells',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '172a51c1-d4d2-4afd-8b1a-a1bffa4dea5d',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Short term debt',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '188bae76-6c3e-4cc2-8abe-3f6ed3664df0',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'PBT',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '1b943b1b-eaac-4e3e-9014-a4817792cb94',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Fixed Assets',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '22094221-37bf-49a6-beb3-c57adf10461e',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Creditors',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '24b9f1c9-f3ad-48b1-ae27-9dfc070aebcc',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Monthly Visits',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '25119bbb-791e-422a-be30-4aa4ed44a7b8',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Total Users',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '29f11641-2f4f-469d-ab3f-1ab70a4a4417',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'EBIT',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2cfe95ec-2a69-446d-9d85-08504c1f04b4',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'CrossSells',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '36fff66f-0a23-4599-aebf-51b9512e30ee',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Daily Active Users',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3d957ff4-d04f-401b-af33-712bed6ef28c',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Amortisation',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3ea85fc3-8302-4b17-80c1-b76a9534715a',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Cost of goods sold',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4031c5d7-8f8d-4226-a55d-80c18abb0dd6',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Mrr',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '40a5c320-ccf8-4b48-84e6-aaa2d1c46038',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Capital commitments',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '41ac5477-9cb7-4148-94cf-00ec017c8f21',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Users',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '45e938fe-ddcb-46cd-a3bf-1275180799b1',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Products',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '465bb276-c430-48bb-815d-c74a60631b28',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Cash generated',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '49a7335b-fa79-47d6-92f8-ddad5e73d2e1',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'CAC',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '49dbee7b-bda1-4ad1-9a4e-354060883a4f',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Clients',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4bd911d0-9871-43a2-abc5-db52259f77f7',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Capital expenditure',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '4ca4ef09-84c1-46e3-82a8-376ac56f7a09',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Orders',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '5249dd4c-c8c3-4b20-ab79-c819f93d5d41',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Customers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '52da8410-2229-4ac6-81d1-63def799dfb7',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Net cash flow',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '579e3b5a-a02c-4179-ac85-be8341cac8b9',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Downloads',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '57a894ab-ed72-4af8-9b0b-52d132dd7642',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Dividend',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6126bcc0-755e-4fd8-9685-39dc739c8cfe',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Taxation',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '62491118-8264-4599-a783-a23e29b4c979',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Visitors',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '62d2b968-221a-4e2f-af6e-47237b875ce1',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Listings',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '65912857-12ef-4ee8-a33a-a00203895274',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Long term liabilities',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6683d768-7230-4ac8-9c24-6c1240084dd3',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Sessions',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '69f11303-5b6a-4966-ad57-cf92fd7473b0',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Facebook Fans',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6d13ee8d-531d-4e8d-b0f2-a8b02e2f79e3',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'LTV',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6d83c5a8-d52a-45e6-be8b-2e0f1256d0f7',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'DAU',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7815b54b-ad59-4615-b003-6cbf6cc965be',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Pageviews',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7819fd32-aed4-465e-9b8a-70dc225252ce',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Sign Ups',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '79eccf4c-73d3-49ad-9420-1f5f5adf72ef',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Stock',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '82fa6e32-2252-41a6-8a0b-8aaa1d97b3cb',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Depreciation',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8a53bbcf-2190-4a4e-9988-e0d19a7565a4',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Merchants',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8c0029ad-0360-4a07-8834-e472d11ef140',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Unique Visitors',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '940459f2-cee6-4d02-80db-f615b25457b7',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Advertisers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'a633565e-3609-4dcd-92b7-ec342417ed94',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Installs',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'af0b5a73-bfd6-489d-a6a9-a6e14a00d7ac',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Members',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b0b08e32-7c9d-4852-bfb4-8f1a34f1d7ea',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'App Downloads',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b46ea71c-1b3c-41f7-af4f-1fd9adb3e9f4',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'New Revenue',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b7009142-5a5f-48d7-8a36-be527c352f78',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Preorders',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b7a11a88-8bb4-49b6-a18c-8c5cdb9a1713',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Daily Views',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bbb2dc25-9125-4721-a25e-0300c0e063e1',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Cost of sales',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'bfb58e64-d322-4d8f-8e71-508c1f7cc9d9',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Debtors',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c072fae7-b2ff-4d6a-8413-0c25486590fa',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Conversion Rate',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c1759843-448c-4ec6-b4d3-0b78a1695247',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Active Users',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'cfbb38b1-f108-4af9-a1d1-4a896a6b34ce',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Gross Profit',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd1cabb36-7f6a-433c-8165-c6cd415285e5',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Subscribers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd3a39c2e-64e5-4527-ad1a-7ac8ce41fc1a',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'PAT',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd4c7cd1c-8639-4508-97fc-b523bff2a080',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Operating expenses',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd691745c-3451-414b-ac53-1fb5c42279ff',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Schools',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e18f1183-702b-4b70-9d97-990d099a6806',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Net Assets',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e2de0c7f-0cae-4873-a840-3726679ffb5b',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Twitter followers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e3c94e33-0efb-400f-bfb9-cfc932e99ae5',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Transactions',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'e82aac40-ec56-4e0d-94ea-8052f5d00198',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Events',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ea48e47b-85be-4cb5-ae5b-0a059d2e4d4c',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'Paying Customers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ec21e094-03c7-4c20-92d0-30cc80a40e83',
    'vocabulary_machine_name' => 'kpi',
  );
  $terms[] = array(
    'name' => 'EBITDA',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f08ed6c0-d683-4bbd-8bc3-9a6fd49a06ba',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Gross Assets',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fad49466-bc8c-48d4-8a10-3eaaf4d258bd',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Long term debt',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fb19556e-d2fb-4852-84fd-71a1618142a4',
    'vocabulary_machine_name' => 'financial_metrics',
  );
  $terms[] = array(
    'name' => 'Email subscribers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'fdc6aa26-1304-4ac9-93b4-c28a273a3f18',
    'vocabulary_machine_name' => 'kpi',
  );
  return $terms;
}
