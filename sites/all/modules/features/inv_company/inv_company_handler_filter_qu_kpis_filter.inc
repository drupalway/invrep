<?php

/**
 * Custom Regular Report filter handler
 */
class inv_company_handler_filter_qu_kpis_filter extends views_handler_filter_in_operator {
  static $results;

  /**
   * Generate options list
   * 
   * @return array|mixed|void
   */
  function get_value_options() {
    $limited_tids = $this->get_limited_tids();
    if (count($limited_tids) > 0) {
      foreach ($limited_tids as $limited_tid => $limited_tid_name) {
        $this->value_options[$limited_tid] = $limited_tid_name;
      }
    }
    if (arg(0) == 'admin' || count($limited_tids) == 0) {
      $this->value_options = array(0 => t('Actual values will be available at run-time'));
    }
  }

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    // Avoid the 'illegal values' Form API error.
    $form['value']['#validated'] = TRUE;
  }

  function query() {
    // Decode the values to restore special chars.
    $this->value = array_map('urldecode', $this->value);
    parent::query();
  }

  /**
   * Helper function to get list of KPIs for current OG
   *
   * @param bool $first
   * @return array
   */
  function get_limited_tids($first = FALSE, $type = 'terms', $gid_filter = TRUE, $og_list = array(), $is_published_report = FALSE) {
    $gid = $_SESSION['og_context']['gid'];
    if (!empty($gid) || !$gid_filter) {
      $terms = array();
      $query = db_select('og_membership', 'ogm');
      if ($gid_filter) {
        $query->condition('ogm.gid', $gid, '=');
      }
      elseif (!empty($og_list) && is_array($og_list) && count($og_list) > 0) {
        $query->condition('ogm.gid', $og_list, 'IN');
      }
      $query->condition('ogm.entity_type', 'node', '=');
      if ($is_published_report) {
        $query->condition('n.status', 1, '=');
      }
      $query->fields('fck', array('field_fc_kpi_tid'));
      $query->fields('fcka', array('field_fc_kpi_actual_value'));
      $query->fields('ttd', array('name'));
      $query->fields('n', array('nid'));
      $query->join('node', 'n', 'n.type = :content_type AND n.nid = ogm.etid AND n.status = :status', array(':content_type' => 'report', ':status' => 1));
      $query->join('field_data_field_fc_kpis', 'fcks', 'fcks.entity_id = n.nid');
      $query->join('field_data_field_fc_kpi', 'fck', 'fck.entity_id = fcks.field_fc_kpis_value');
      $query->join('field_data_field_fc_kpi_actual', 'fcka', 'fcka.entity_id = fcks.field_fc_kpis_value');
      $query->join('taxonomy_term_data', 'ttd', 'ttd.tid = fck.field_fc_kpi_tid');
      $query->orderBy('ttd.weight', 'ASC');
      $query->orderBy('ttd.name', 'ASC');
      $query->distinct();
      if ($first) {
        return $query->execute()->fetchField();
      }
      else {
        $result = $query->execute();
        foreach ($result as $row) {
          switch($type) {
            case'terms':
              $terms[$row->field_fc_kpi_tid] = $row->name;
              break;
            case'reports':
              $terms[$row->field_fc_kpi_tid]['reports'][] = $row->nid;
              $terms[$row->field_fc_kpi_tid][$row->nid]['actual'] = $row->field_fc_kpi_actual_value;
              break;
          }
        }
        return $terms;
      }
    }
    else {
      return array();
    }
  }
}

/**
 * Custom Regular Report Financial KPIs filter handler
 */
class inv_company_handler_filter_financial_kpis_filter extends views_handler_filter_in_operator {
  static $results;

  /**
   * Generate options list
   *
   * @return array|mixed|void
   */
  function get_value_options() {
    $limited_tids = $this->get_limited_tids();
    if (count($limited_tids) > 0) {
      foreach ($limited_tids as $limited_tid => $limited_tid_name) {
        $this->value_options[$limited_tid] = $limited_tid_name;
      }
    }
    if (arg(0) == 'admin' || count($limited_tids) == 0) {
      $this->value_options = array(0 => t('Actual values will be available at run-time'));
    }
  }

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    // Avoid the 'illegal values' Form API error.
    $form['value']['#validated'] = TRUE;
    if (arg(0) != 'admin' && !empty($form['value']['#options'][0])) {
      $form['#attributes']['class'][] = 'hidden';
    }
  }

  function query() {
    // Decode the values to restore special chars.
    $this->value = array_map('urldecode', $this->value);
    parent::query();
  }

  /**
   * Helper function to get list of KPIs for current OG
   *
   * @param bool $first
   * @return array
   */
  function get_limited_tids($first = FALSE) {
    $gid = $_SESSION['og_context']['gid'];
    if (!empty($gid)) {
      $terms = array();
      $query = db_select('og_membership', 'ogm');
      $query->condition('ogm.gid', $gid, '=');
      $query->condition('ogm.entity_type', 'node', '=');
      $query->fields('fcfm', array('field_fc_financial_metric_tid'));
      $query->fields('ttd', array('name'));
      $query->join('node', 'n', 'n.type = :content_type AND n.nid = ogm.etid AND n.status = :status', array(':content_type' => 'report', ':status' => 1));
      $query->join('field_data_field_fc_financial_kpis', 'fcfk', 'fcfk.entity_id = n.nid');
      $query->join('field_data_field_fc_financial_metric', 'fcfm', 'fcfm.entity_id = fcfk.field_fc_financial_kpis_value');
      $query->join('taxonomy_term_data', 'ttd', 'ttd.tid = fcfm.field_fc_financial_metric_tid');
      $query->orderBy('ttd.weight', 'ASC');
      $query->orderBy('ttd.name', 'ASC');
      $query->distinct();
      if ($first) {
        return $query->execute()->fetchField();
      }
      else {
        $result = $query->execute();
        foreach ($result as $row) {
          $terms[$row->field_fc_financial_metric_tid] = $row->name;
        }
        return $terms;
      }
    }
    else {
      return array();
    }
  }
}