<?php
/**
 * @file
 * inv_my_portfolio_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function inv_my_portfolio_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Portfolio Composition',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '61a94e0e-b462-46e5-aed4-46cab5937192',
  'type' => 'article',
  'language' => 'und',
  'created' => 1384448497,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '1ae97447-1d70-47c6-9802-5a9b395989a1',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<?php
$view = \'swf_clone_of_og_user_groups\';
?>

<div style="width:30%;float:left;">
<?php
print views_embed_view($view, $display_id = \'block_1\');
?>
</div>
<div style="width:30%;float:left;">
<?php
print views_embed_view($view, $display_id = \'block_5\');
?>
</div>

<div style="width:30%;float:left;">
<?php
print views_embed_view($view, $display_id = \'block_3\');
?>
</div>

',
        'summary' => '',
        'format' => 'php_code',
      ),
    ),
  ),
  'field_tags' => array(),
  'field_image' => array(),
  'field_more_images' => array(),
  'og_group_ref' => array(),
  'field_file_upload_art' => array(),
  'rdf_mapping' => array(
    'field_image' => array(
      'predicates' => array(
        0 => 'og:image',
        1 => 'rdfs:seeAlso',
      ),
      'type' => 'rel',
    ),
    'field_tags' => array(
      'predicates' => array(
        0 => 'dc:subject',
      ),
      'type' => 'rel',
    ),
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-11-14 17:01:37 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Investor Dashboard',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'c63b888d-25b9-4763-a370-34304f74adba',
  'type' => 'article',
  'language' => 'und',
  'created' => 1384452923,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '9133253b-84b3-4508-bc31-b46735ab2f8a',
  'revision_uid' => 1,
  'body' => array(),
  'field_tags' => array(),
  'field_image' => array(),
  'field_more_images' => array(),
  'og_group_ref' => array(),
  'field_file_upload_art' => array(),
  'rdf_mapping' => array(
    'field_image' => array(
      'predicates' => array(
        0 => 'og:image',
        1 => 'rdfs:seeAlso',
      ),
      'type' => 'rel',
    ),
    'field_tags' => array(
      'predicates' => array(
        0 => 'dc:subject',
      ),
      'type' => 'rel',
    ),
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-11-14 18:15:23 +0000',
);
  return $nodes;
}
