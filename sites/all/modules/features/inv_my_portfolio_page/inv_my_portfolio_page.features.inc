<?php
/**
 * @file
 * inv_my_portfolio_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function inv_my_portfolio_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function inv_my_portfolio_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
