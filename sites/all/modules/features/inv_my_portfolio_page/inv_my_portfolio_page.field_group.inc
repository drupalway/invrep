<?php
/**
 * @file
 * inv_my_portfolio_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function inv_my_portfolio_page_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_info|node|startup_groups|form';
  $field_group->group_name = 'group_basic_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'startup_groups';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic Info',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_website',
      2 => 'field_local_currency',
      3 => 'field_revenue_model',
      4 => 'field_target_customers',
      5 => 'field_country',
      6 => 'title',
      7 => 'field_company_tags',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basic_info|node|startup_groups|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_documents|node|startup_groups|form';
  $field_group->group_name = 'group_documents';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'startup_groups';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Documents',
    'weight' => '2',
    'children' => array(
      0 => 'field_investment_agreement',
      1 => 'field_presentations_upload',
      2 => 'field_budget_upload',
      3 => 'field_cap_table',
      4 => 'field_liquidation_curve',
      5 => 'field_promotional_video',
      6 => 'field_accounts',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Documents',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_documents|node|startup_groups|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_more_info|node|startup_groups|form';
  $field_group->group_name = 'group_more_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'startup_groups';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'More Info',
    'weight' => '1',
    'children' => array(
      0 => 'field_headquaters',
      1 => 'field_logo',
      2 => 'field_blog',
      3 => 'field_twitter',
      4 => 'field_email',
      5 => 'field_crunchbase',
      6 => 'field_funds_raised',
      7 => 'field_funds_targeted',
      8 => 'field_fundraising',
      9 => 'field_company_registration_numbe',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'More Info',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_more_info|node|startup_groups|form'] = $field_group;

  return $export;
}
