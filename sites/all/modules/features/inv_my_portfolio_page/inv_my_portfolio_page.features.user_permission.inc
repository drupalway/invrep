<?php
/**
 * @file
 * inv_my_portfolio_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function inv_my_portfolio_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer quicktabs'.
  $permissions['administer quicktabs'] = array(
    'name' => 'administer quicktabs',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'quicktabs',
  );

  return $permissions;
}
