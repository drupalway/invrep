<?php
/**
 * @file
 * inv_my_portfolio_page.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function inv_my_portfolio_page_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'investor_dashboard';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Investor_Dashboard';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'og_user_groups',
      'display' => 'block_6',
      'args' => '',
      'title' => 'Descriptions',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'bid' => 'views_delta_651d92564e078346d7cb56db4597e8af',
      'hide_title' => 1,
      'title' => 'Summary',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'vid' => 'highchart_bubble_custom',
      'display' => 'block_investor_dashboard',
      'args' => '',
      'title' => 'Orbit Chart',
      'weight' => '-99',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'number_of_companies_and_reports',
      'display' => 'block_5',
      'args' => '',
      'title' => 'Cash runway',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'vid' => 'number_of_companies_and_reports',
      'display' => 'block_2',
      'args' => '',
      'title' => 'Reporting',
      'weight' => '-96',
      'type' => 'view',
    ),
    5 => array(
      'vid' => 'number_of_companies_and_reports',
      'display' => 'block_7',
      'args' => '',
      'title' => 'Punctuality',
      'weight' => '-95',
      'type' => 'view',
    ),
    6 => array(
      'vid' => 'my_portoflio_comment_by_investor',
      'display' => 'block',
      'args' => '',
      'title' => 'Comments',
      'weight' => '-94',
      'type' => 'view',
    ),
    7 => array(
      'nid' => '232',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => 'Portfolio',
      'weight' => '-94',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'ui_tabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array(
    'history' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Cash runway');
  t('Comments');
  t('Descriptions');
  t('Investor_Dashboard');
  t('Orbit Chart');
  t('Portfolio');
  t('Punctuality');
  t('Reporting');
  t('Summary');

  $export['investor_dashboard'] = $quicktabs;

  return $export;
}
