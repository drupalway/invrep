<?php
/**
 * @file
 * inv_menus_and_actions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function inv_menus_and_actions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer menu'.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'menu',
  );

  return $permissions;
}
