<?php
/**
 * @file
 * inv_menus_and_actions.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function inv_menus_and_actions_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_free-trial:http://www.invrep.co/user/register
  $menu_links['main-menu_free-trial:http://www.invrep.co/user/register'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://www.invrep.co/user/register',
    'router_path' => '',
    'link_title' => 'Free trial',
    'options' => array(
      'attributes' => array(
        'title' => 'Register',
      ),
      'identifier' => 'main-menu_free-trial:http://www.invrep.co/user/register',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_learn-more:node/5
  $menu_links['main-menu_learn-more:node/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Learn More',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_learn-more:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_pricing:node/6
  $menu_links['main-menu_pricing:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'Pricing',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_pricing:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-main-menu-logged-in_add-my-company:node/add/startup-groups
  $menu_links['menu-main-menu-logged-in_add-my-company:node/add/startup-groups'] = array(
    'menu_name' => 'menu-main-menu-logged-in',
    'link_path' => 'node/add/startup-groups',
    'router_path' => 'node/add/startup-groups',
    'link_title' => 'Add My Company',
    'options' => array(
      'attributes' => array(
        'title' => '30 day free trial for all companies',
      ),
      'identifier' => 'menu-main-menu-logged-in_add-my-company:node/add/startup-groups',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
  );
  // Exported menu link: menu-main-menu-logged-in_home:http://www.invrep.co/
  $menu_links['menu-main-menu-logged-in_home:http://www.invrep.co/'] = array(
    'menu_name' => 'menu-main-menu-logged-in',
    'link_path' => 'http://www.invrep.co/',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => 'Home',
      ),
      'identifier' => 'menu-main-menu-logged-in_home:http://www.invrep.co/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-main-menu-logged-in_investor-dashboard:investor-dashboard
  $menu_links['menu-main-menu-logged-in_investor-dashboard:investor-dashboard'] = array(
    'menu_name' => 'menu-main-menu-logged-in',
    'link_path' => 'investor-dashboard',
    'router_path' => 'investor-dashboard',
    'link_title' => 'Investor Dashboard',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-main-menu-logged-in_investor-dashboard:investor-dashboard',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add My Company');
  t('Free trial');
  t('Home');
  t('Investor Dashboard');
  t('Learn More');
  t('Pricing');


  return $menu_links;
}
