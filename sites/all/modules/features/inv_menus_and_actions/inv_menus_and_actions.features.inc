<?php
/**
 * @file
 * inv_menus_and_actions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function inv_menus_and_actions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
