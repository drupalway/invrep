<?php
/**
 * @file
 * inv_menus_and_actions.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function inv_menus_and_actions_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-main-menu-logged-in.
  $menus['menu-main-menu-logged-in'] = array(
    'menu_name' => 'menu-main-menu-logged-in',
    'title' => 'Main Menu Logged In',
    'description' => 'Menu system for logged in users only',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Main Menu Logged In');
  t('Main menu');
  t('Menu system for logged in users only');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');


  return $menus;
}
