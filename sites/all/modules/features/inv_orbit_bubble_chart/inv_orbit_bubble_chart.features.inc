<?php
/**
 * @file
 * inv_orbit_bubble_chart.features.inc
 */

/**
 * Implements hook_views_api().
 */
function inv_orbit_bubble_chart_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
