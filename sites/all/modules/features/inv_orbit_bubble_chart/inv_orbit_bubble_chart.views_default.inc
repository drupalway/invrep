<?php
/**
 * @file
 * inv_orbit_bubble_chart.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function inv_orbit_bubble_chart_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'google_bubble_chart_test';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Google Bubble Chart Test';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['dashboard_elements'] = array(
    'element_1' => array(
      'name' => 'element_1',
      'element_type' => 'BubbleChart',
      'label' => 'Element 1',
      'options' => array(
        'element_type' => '',
        'name' => 'element_1',
        'ui_name' => '',
        'custom_label' => 0,
        'label' => '',
        'element_label_colon' => FALSE,
        'element_type_enable' => 0,
        'element_class_enable' => 0,
        'element_class' => '',
        'element_label_type_enable' => 0,
        'element_label_type' => '',
        'element_label_class_enable' => 0,
        'element_label_class' => '',
        'element_wrapper_type_enable' => 0,
        'element_wrapper_type' => '',
        'element_wrapper_class_enable' => 0,
        'element_wrapper_class' => '',
        'element_default_classes' => 1,
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'absolute' => 0,
          'replace_spaces' => 0,
          'external' => 0,
          'path_case' => 'none',
          'link_class' => '',
          'alt' => '',
          'rel' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'more_link' => 0,
          'more_link_text' => '',
          'more_link_path' => '',
          'html' => 0,
          'strip_tags' => 0,
          'preserve_tags' => '',
          'trim_whitespace' => 0,
          'nl2br' => 0,
        ),
        'empty' => '',
        'empty_zero' => 0,
        'hide_empty' => 0,
        'hide_alter_empty' => 1,
        'width' => '800',
        'height' => '400',
        'animation' => array(
          'duration' => '600',
          'easing' => 'linear',
        ),
        'chart_area' => array(
          'width' => '600',
          'height' => '300',
        ),
        'bubble' => array(
          'opacity' => '1',
        ),
        'h_axis' => array(
          'title' => 'Traction: Monthly revenue',
          'direction' => '1',
          'format' => '#,###',
          'gridlines' => array(
            'color' => '#eee',
            'count' => '5',
          ),
          'minor_gridlines' => array(
            'color' => '#f1f1f1',
            'count' => '2',
          ),
        ),
        'v_axis' => array(
          'title' => 'Cash burn',
          'direction' => '1',
          'format' => '#,###',
          'gridlines' => array(
            'color' => '#eee',
            'count' => '5',
          ),
          'minor_gridlines' => array(
            'color' => '#f1f1f1',
            'count' => '2',
          ),
        ),
        'legend' => array(
          'position' => 'bottom',
          'alignment' => 'start',
        ),
        'tooltip' => array(
          'trigger' => 'hover',
          'text' => 'value',
        ),
      ),
    ),
  );
  $handler->display->display_options['formats'] = array(
    'field_quarter_end_date' => array(
      'format' => 'DateFormat',
      'column' => 'field_quarter_end_date',
      'options' => array(
        'pattern' => 'MMM yy',
      ),
    ),
    'field_monthly_cash_burn' => array(
      'format' => 'NumberFormat',
      'column' => 'field_monthly_cash_burn',
      'options' => array(
        'groupingSymbol' => ',',
        'fractionDigits' => 0,
        'prefix' => '',
        'suffix' => '',
      ),
    ),
    'field_monthly_revenue' => array(
      'format' => 'NumberFormat',
      'column' => 'field_monthly_revenue',
      'options' => array(
        'groupingSymbol' => ',',
        'fractionDigits' => 2,
        'prefix' => '',
        'suffix' => '',
      ),
    ),
    'field_position_cash_in_bank' => array(
      'format' => 'NumberFormat',
      'column' => 'field_position_cash_in_bank',
      'options' => array(
        'groupingSymbol' => ',',
        'fractionDigits' => 0,
        'prefix' => '',
        'suffix' => '',
      ),
    ),
  );
  $handler->display->display_options['title'] = 'Portfolio Dashboard Analytics';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    2 => '2',
    4 => '4',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Go';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'field_quarter_end_date_value' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'field_monthly_revenue_value' => array(
      'bef_format' => 'bef_slider',
      'slider_options' => array(
        'bef_slider_min' => '0',
        'bef_slider_max' => '1500000',
        'bef_slider_step' => '25000',
        'bef_slider_animate' => 'fast',
        'bef_slider_orientation' => 'horizontal',
      ),
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'field_monthly_cash_burn_value_1' => array(
      'bef_format' => 'bef_slider',
      'slider_options' => array(
        'bef_slider_min' => '-250000',
        'bef_slider_max' => '500000',
        'bef_slider_step' => '25000',
        'bef_slider_animate' => 'fast',
        'bef_slider_orientation' => 'horizontal',
      ),
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'field_position_cash_in_bank_value' => array(
      'bef_format' => 'bef_slider',
      'slider_options' => array(
        'bef_slider_min' => '-500000',
        'bef_slider_max' => '2500000',
        'bef_slider_step' => '25000',
        'bef_slider_animate' => 'fast',
        'bef_slider_orientation' => 'horizontal',
      ),
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_chart_tools_dashboard';
  $handler->display->display_options['style_options']['info'] = array(
    'field_quarter_end_date' => array(
      'type' => 'string',
      'role' => 'tooltip',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'field_monthly_revenue' => array(
      'type' => 'number',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'field_monthly_cash_burn' => array(
      'type' => 'number',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'og_group_ref' => array(
      'type' => 'string',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'field_position_cash_in_bank' => array(
      'type' => 'number',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
  );
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<p class="tooltip-hover" style="margin-bottom:0px;float:right; font-size:0.9em;">Bubble Size &#187; Cash in Bank (not to scale) &bull; <a style="font-size:0.9em;" href="http://www.invrep.co/content/types-equity-funding-and-considerations#ISOchart" target="_blank">View guidance chart</a><img class="tooltip-image" src="http://www.invrep.co/sites/default/files/styles/adaptive_swf/adaptive-image/public/Invrep_Startup-Orbits_Entry-Point-By-Funding-Strategy_0.png"></p>';
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  /* Field: Content: Period End Date */
  $handler->display->display_options['fields']['field_quarter_end_date']['id'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['field'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_quarter_end_date']['settings'] = array(
    'format_type' => 'very_short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Traction: Monthly Revenue */
  $handler->display->display_options['fields']['field_monthly_revenue']['id'] = 'field_monthly_revenue';
  $handler->display->display_options['fields']['field_monthly_revenue']['table'] = 'field_data_field_monthly_revenue';
  $handler->display->display_options['fields']['field_monthly_revenue']['field'] = 'field_monthly_revenue';
  $handler->display->display_options['fields']['field_monthly_revenue']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_monthly_revenue']['settings'] = array(
    'thousand_separator' => ',',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Monthly Profit or (Cash Burn) */
  $handler->display->display_options['fields']['field_monthly_cash_burn']['id'] = 'field_monthly_cash_burn';
  $handler->display->display_options['fields']['field_monthly_cash_burn']['table'] = 'field_data_field_monthly_cash_burn';
  $handler->display->display_options['fields']['field_monthly_cash_burn']['field'] = 'field_monthly_cash_burn';
  $handler->display->display_options['fields']['field_monthly_cash_burn']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_monthly_cash_burn']['settings'] = array(
    'thousand_separator' => ',',
    'prefix_suffix' => 0,
  );
  /* Field: Content: Groups audience */
  $handler->display->display_options['fields']['og_group_ref']['id'] = 'og_group_ref';
  $handler->display->display_options['fields']['og_group_ref']['table'] = 'og_membership';
  $handler->display->display_options['fields']['og_group_ref']['field'] = 'og_group_ref';
  $handler->display->display_options['fields']['og_group_ref']['label'] = 'Company';
  $handler->display->display_options['fields']['og_group_ref']['element_label_type'] = 'em';
  $handler->display->display_options['fields']['og_group_ref']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['og_group_ref']['delta_offset'] = '0';
  /* Field: Content: Cash in Bank */
  $handler->display->display_options['fields']['field_position_cash_in_bank']['id'] = 'field_position_cash_in_bank';
  $handler->display->display_options['fields']['field_position_cash_in_bank']['table'] = 'field_data_field_position_cash_in_bank';
  $handler->display->display_options['fields']['field_position_cash_in_bank']['field'] = 'field_position_cash_in_bank';
  $handler->display->display_options['fields']['field_position_cash_in_bank']['type'] = 'number_unformatted';
  $handler->display->display_options['fields']['field_position_cash_in_bank']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Contextual filter: User: Group membership (og_user_node) */
  $handler->display->display_options['arguments']['og_user_node_target_id']['id'] = 'og_user_node_target_id';
  $handler->display->display_options['arguments']['og_user_node_target_id']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['og_user_node_target_id']['field'] = 'og_user_node_target_id';
  $handler->display->display_options['arguments']['og_user_node_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['og_user_node_target_id']['default_argument_type'] = 'og_user_groups';
  $handler->display->display_options['arguments']['og_user_node_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['og_user_node_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['og_user_node_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['og_user_node_target_id']['break_phrase'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: OG membership: State */
  $handler->display->display_options['filters']['state']['id'] = 'state';
  $handler->display->display_options['filters']['state']['table'] = 'og_membership';
  $handler->display->display_options['filters']['state']['field'] = 'state';
  $handler->display->display_options['filters']['state']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Content: Period End Date (field_quarter_end_date) */
  $handler->display->display_options['filters']['field_quarter_end_date_value']['id'] = 'field_quarter_end_date_value';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['field'] = 'field_quarter_end_date_value';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['operator'] = '>';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_quarter_end_date_value']['expose']['operator_id'] = 'field_quarter_end_date_value_op';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['expose']['label'] = 'Data from';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['expose']['operator'] = 'field_quarter_end_date_value_op';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['expose']['identifier'] = 'field_quarter_end_date_value';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['expose']['remember_roles'] = array(
    2 => 0,
    4 => '4',
    1 => 0,
    3 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_quarter_end_date_value']['default_date'] = '-270 days';
  $handler->display->display_options['filters']['field_quarter_end_date_value']['year_range'] = '-5:+1';
  /* Filter criterion: Content: Monthly Profit or (Cash Burn) (field_monthly_cash_burn) */
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['id'] = 'field_monthly_cash_burn_value';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['table'] = 'field_data_field_monthly_cash_burn';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['field'] = 'field_monthly_cash_burn_value';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['operator'] = 'not empty';
  /* Filter criterion: Content: Traction: Monthly Revenue (field_monthly_revenue) */
  $handler->display->display_options['filters']['field_monthly_revenue_value']['id'] = 'field_monthly_revenue_value';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['table'] = 'field_data_field_monthly_revenue';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['field'] = 'field_monthly_revenue_value';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['operator'] = 'between';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['value']['min'] = '0';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['value']['max'] = '275000';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_monthly_revenue_value']['expose']['operator_id'] = 'field_monthly_revenue_value_op';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['expose']['label'] = 'Monthly Revenue';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['expose']['operator'] = 'field_monthly_revenue_value_op';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['expose']['identifier'] = 'field_monthly_revenue_value';
  $handler->display->display_options['filters']['field_monthly_revenue_value']['expose']['remember_roles'] = array(
    2 => 0,
    4 => '4',
    1 => 0,
    3 => 0,
    5 => 0,
  );
  /* Filter criterion: Content: Monthly Profit or (Cash Burn) (field_monthly_cash_burn) */
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['id'] = 'field_monthly_cash_burn_value_1';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['table'] = 'field_data_field_monthly_cash_burn';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['field'] = 'field_monthly_cash_burn_value';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['operator'] = 'between';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['value']['min'] = '-75000';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['value']['max'] = '75000';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['expose']['operator_id'] = 'field_monthly_cash_burn_value_1_op';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['expose']['label'] = 'Monthly Cash Profit';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['expose']['operator'] = 'field_monthly_cash_burn_value_1_op';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['expose']['identifier'] = 'field_monthly_cash_burn_value_1';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value_1']['expose']['remember_roles'] = array(
    2 => 0,
    4 => '4',
    1 => 0,
    3 => 0,
    5 => 0,
  );
  /* Filter criterion: Content: Cash in Bank (field_position_cash_in_bank) */
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['id'] = 'field_position_cash_in_bank_value';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['table'] = 'field_data_field_position_cash_in_bank';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['field'] = 'field_position_cash_in_bank_value';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['operator'] = 'between';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['value']['min'] = '-250000';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['value']['max'] = '475000';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['expose']['operator_id'] = 'field_position_cash_in_bank_value_op';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['expose']['label'] = 'Cash in Bank';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['expose']['operator'] = 'field_position_cash_in_bank_value_op';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['expose']['identifier'] = 'field_position_cash_in_bank_value';
  $handler->display->display_options['filters']['field_position_cash_in_bank_value']['expose']['remember_roles'] = array(
    2 => 0,
    4 => '4',
    1 => 0,
    3 => 0,
    5 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<div id="ugh">Ugh - we\'re sorry that some charts and tables don\'t look great on smaller devices
We\'re trying to make this better. Until then, please use a larger device and bear with us. Thanks!</div>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<img src="http://www.invrep.co/sites/default/files/styles/adaptive_swf/adaptive-image/public/Demo_Bubble%20Chart_0.png" class="adaptive-image" alt="Invrep Orbit Chart. Demo Loading..." title="Invrep Orbit Chart">
<p><i>This is for demonstration only. A real chart will appear here when you add companies, and they upload Quarterly Reports</i><p>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['path'] = 'my-portfolio';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Summary Orbit Chart: Traction vs. Profitability & Cash';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'views_chart_tools_dashboard';
  $handler->display->display_options['style_options']['info'] = array(
    'field_quarter_end_date' => array(
      'type' => 'string',
      'role' => 'tooltip',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'field_monthly_revenue' => array(
      'type' => 'number',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'field_monthly_cash_burn' => array(
      'type' => 'number',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'og_group_ref' => array(
      'type' => 'string',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
    'field_position_cash_in_bank' => array(
      'type' => 'number',
      'role' => '',
      'group' => 0,
      'modifier' => '',
      'aggregation' => '',
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<div id="ugh">Ugh - we\'re sorry that some charts and tables don\'t look great on smaller devices
We\'re trying to make this better. Until then, please use a larger device and bear with us. Thanks!</div>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: OG membership: OG membership from Content */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['required'] = TRUE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: OG membership: State */
  $handler->display->display_options['filters']['state']['id'] = 'state';
  $handler->display->display_options['filters']['state']['table'] = 'og_membership';
  $handler->display->display_options['filters']['state']['field'] = 'state';
  $handler->display->display_options['filters']['state']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Content: Monthly Profit or (Cash Burn) (field_monthly_cash_burn) */
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['id'] = 'field_monthly_cash_burn_value';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['table'] = 'field_data_field_monthly_cash_burn';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['field'] = 'field_monthly_cash_burn_value';
  $handler->display->display_options['filters']['field_monthly_cash_burn_value']['operator'] = 'not empty';
  $handler->display->display_options['block_description'] = 'View: Bubble Chart for Company Dashboard Page';
  $export['google_bubble_chart_test'] = $view;

  return $export;
}
