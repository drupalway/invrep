<?php
/**
 * @file
 * invrep_elibrary_press.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function invrep_elibrary_press_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'eLibrary and Resources',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '0efaa7c0-0af2-4453-923b-a255d0ff4f44',
  'type' => 'article',
  'language' => 'und',
  'created' => 1384355508,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '6cf68f40-1a4a-4934-9b33-c53fdf6a9f55',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<h3>eLibrary &bull; Resources</h3>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<h3>eLibrary • Resources</h3>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_tags' => array(),
  'field_image' => array(),
  'field_more_images' => array(),
  'og_group_ref' => array(),
  'field_file_upload_art' => array(),
  'rdf_mapping' => array(
    'field_image' => array(
      'predicates' => array(
        0 => 'og:image',
        1 => 'rdfs:seeAlso',
      ),
      'type' => 'rel',
    ),
    'field_tags' => array(
      'predicates' => array(
        0 => 'dc:subject',
      ),
      'type' => 'rel',
    ),
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-11-13 15:11:48 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Press',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '231c5cd1-bf65-41d1-90c5-aea3608f2364',
  'type' => 'article',
  'language' => 'und',
  'created' => 1370530055,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '86142bc3-23b7-4620-844c-865d35c05c10',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<h3>eLibrary &bull; Resources &bull; Blog &bull; Press</h3>

',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<h3>eLibrary • Resources • Blog • Press</h3>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_tags' => array(),
  'field_image' => array(),
  'field_more_images' => array(),
  'og_group_ref' => array(),
  'field_file_upload_art' => array(),
  'rdf_mapping' => array(
    'field_image' => array(
      'predicates' => array(
        0 => 'og:image',
        1 => 'rdfs:seeAlso',
      ),
      'type' => 'rel',
    ),
    'field_tags' => array(
      'predicates' => array(
        0 => 'dc:subject',
      ),
      'type' => 'rel',
    ),
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-06-06 14:47:35 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'eLibrary',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '11057b8b-35d1-4397-9074-6508083a5fc9',
  'type' => 'page',
  'language' => 'und',
  'created' => 1367514437,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'e92e43a0-cbef-45fb-a132-3a9ed0195133',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<?php
drupal_add_library(\'system\', \'ui.accordion\');
drupal_add_js(\'jQuery(document).ready(function(){jQuery("#accordion").accordion({collapsible: true, autoHeight: false});});\', \'inline\');
?>

<h3>Resources to deliver world class governance and investor relations</h3>

<div style="clear:left;">
</div>

<div id="accordion" style="clear:left;">
<h2>Using the Invrep Platform</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/who-uses-invrep-investor-reporting-platform">Who is it for?</a> </p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="invrep-investor-reporting-platform-quick-start-guide">Quick start guide</a> </p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="step-step-guide-using-invrep-platform">Detailed guide</a> </p></li>
</ul>
</div>

<h2>Investor Reporting - introduction</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/guide-investor-reporting-and-board-reporting-startups-and-smes">A guide to investor reporting and board reporting for startups and SMEs</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump">Why is investor reporting important </p></li>
</ul>
</div>

<h2>The Board</h2>
<div><ul>
<li><p class="icon plus lefty"> </p><p class="bump">Board composition and its purpose </p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/board-director-meetings-basics">Board of Director meetings: the basics</a></p></li>
<li><p class="icon team lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/board-meeting-notes-template">Board meeting - notes template (1-pager)</a></p></li>
<li><p class="icon team lefty"> </p><p class="bump">NEDs - how to choose one and what they are for </p></li>
</ul>
</div>

<h2>Budget</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump">Budgets – an introduction </p></li>
<li><p class="icon plus lefty"> </p><p class="bump">Difference between a Budget and a Strategic review </p></li>
<li><p class="icon plus lefty"> </p><p class="bump">KPIs and CSFs  - and how to use them </p></li>
<li><p class="icon team lefty"> </p><p class="bump">Template covering what should be included in a budget (and what should be left out!) </p></li>
<li><p class="icon team lefty"> </p><p class="bump">Budget preparation timetable </p></li>
<li><p class="icon warning lefty"> </p><p class="bump">Pitfalls of budgeting, target setting and when to prepare a re-forecast </p></li>
</ul>
</div>

<h2>Strategic review</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump">What is a strategic review, and when should a Strategic Review be conducted </p></li>
<li><p class="icon warning lefty"> </p><p class="bump">How to conduct a Strategic Review: who’s involved and what should be covered </p></li>
<li><p class="icon warning lefty"> </p><p class="bump">Strategic review case study</p></li>
<li><p class="icon warning lefty"> </p><p class="bump">Strategic Review fact pack </p></li>
</ul>
</div>

<h2>Raising funds</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/is-your-startup-worth-1m">Is your startup worth $1m? (20 question quiz)</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/traction-raise-1m">Traction to raise $1m</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/types-equity-funding-and-considerations">Types of equity funding and considerations </a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump">Types of debt funding and considerations </p></li>
</ul>
</div>

<h2>The Team</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump">CEO types and transition </p></li>
<li><p class="icon plus lefty"> </p><p class="bump">Recruitment</p></li>
</ul>
</div>

<h2>The Market</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.sequoiacap.com/grove/posts/afed-pricing-your-product" target="blank">Pricing strategies (external - Sequoia Capital)</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump">Appraising market risks </p></li>
</ul>
</div>

<h2>Business Models</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.ivp.com/assets/pdf/ivp_freemium_paper.pdf" target="_blank">Freemium business models (external)</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/saas-metrics">Saas metrics</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://500.co/2013/09/17/distribution-metrics-for-dummies-the-science-of-profitability/" target="_blank">CAC & LTV (external - 500 Startups)</a></p></li>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/kpis-and-how-use-them">KPIs and how to use them</a></li>
</ul>
</div>

<h2>Financial Modelling</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump">What is a financial model for – things to consider </p></li>
<li><p class="icon plus lefty"> </p><p class="bump">A basic financial model – operating model (Google Spreadsheet) </p></li>
<li><p class="icon warning lefty"> </p><p class="bump">Detailed financial model - with VC structure (Excel download) </p></li>
</ul>
</div>

<h2>Cap table</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/what-cap-table-and-how-prepare-one">What is a cap table, and how to prepare one</a></p></li>
<li><p class="icon warning lefty"> </p><p class="bump"> We can assist constructing your cap table for you </p></li>
</ul>
</div>

<h2>Liquidation curves</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/what-are-liquidation-curves-and-how-construct-them">What are liquidation curves, and how to prepare them</a></p></li>
<li><p class="icon warning lefty"> </p><p class="bump"> We can assist constructing your liquidation curve for you </p></li>
</ul>
</div>

<h2>The Community</h2>
<div>
<ul>
<li><p class="icon plus lefty"> </p><p class="bump"><a href="http://www.invrep.co/content/great-entrepreneurial-blogs">Some great entrepreneurial blogs to follow </a></p></li>
</ul>
</div>
</div>
<p> </p>

<p> </p>

<div id="key" style="clear:left;">
<p><span class="icon plus lefty"> </span><i> - free</i></p>
<p><span class="icon team lefty"> </span><i> - members only - but it\'s free to <a href="http://www.invrep.co/user/register">join</a></i></p>
<p><span class="icon warning lefty"> </span><i> - premium. See <a href="http://www.invrep.co/content/plans">plans</a></i></p>
</div>

<div>
<p>Resources are being added all the time – bookmark this page and/or <a href="contactus">request</a> a topic to be covered</p>
</div>
<p> </p>',
        'summary' => '',
        'format' => 'php_code',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-05-02 17:07:17 +0000',
);
  return $nodes;
}
