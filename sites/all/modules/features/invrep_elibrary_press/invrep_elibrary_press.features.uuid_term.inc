<?php
/**
 * @file
 * invrep_elibrary_press.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function invrep_elibrary_press_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Organisational structure',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0768baf8-3e53-4583-b15d-e90aabdacfa6',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Angel investing',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '204a4372-7253-47f0-ad08-2441d100c543',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Startups',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '224e64cc-98d7-4ec5-a1bd-ce1edc2ac8d8',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Crowdfunding',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '419b5b5c-cb36-4492-97f6-065c8feb2835',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Angel',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '71b99ed6-6bd1-4609-8aca-ba2eec395906',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Blog',
    'description' => 'A blog post',
    'format' => 'full_html',
    'weight' => 1,
    'uuid' => '8d9ab623-9f78-4e06-ae39-8f30dd31df28',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Investor reporting',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'b7c1e860-6e17-42ef-9645-448c6c1bc3a0',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'Press',
    'description' => 'Press article or clipping',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'beaddec2-1b02-4171-9101-47bfc6014aaa',
    'vocabulary_machine_name' => 'tags',
  );
  return $terms;
}
