<?php
/**
 * @file
 * invrep_elibrary_press.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function invrep_elibrary_press_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_tagclouds-configuration:admin/config/content/tagclouds
  $menu_links['management_tagclouds-configuration:admin/config/content/tagclouds'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/content/tagclouds',
    'router_path' => 'admin/config/content/tagclouds',
    'link_title' => 'TagClouds configuration',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure the tag clouds. Set the order, the number of tags, and the depth of the clouds.',
      ),
      'identifier' => 'management_tagclouds-configuration:admin/config/content/tagclouds',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_content-authoring:admin/config/content',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('TagClouds configuration');


  return $menu_links;
}
