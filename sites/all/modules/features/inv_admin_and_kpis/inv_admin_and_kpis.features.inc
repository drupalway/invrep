<?php
/**
 * @file
 * inv_admin_and_kpis.features.inc
 */

/**
 * Implements hook_views_api().
 */
function inv_admin_and_kpis_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
