<?php
/**
 * @file
 * wysiwyg_and_in_place_editing.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function wysiwyg_and_in_place_editing_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_wysiwyg-profiles:admin/config/content/wysiwyg
  $menu_links['management_wysiwyg-profiles:admin/config/content/wysiwyg'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/content/wysiwyg',
    'router_path' => 'admin/config/content/wysiwyg',
    'link_title' => 'Wysiwyg profiles',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure client-side editors.',
      ),
      'identifier' => 'management_wysiwyg-profiles:admin/config/content/wysiwyg',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_content-authoring:admin/config/content',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Wysiwyg profiles');


  return $menu_links;
}
