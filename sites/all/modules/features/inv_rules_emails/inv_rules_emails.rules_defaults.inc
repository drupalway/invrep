<?php
/**
 * @file
 * inv_rules_emails.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function inv_rules_emails_default_rules_configuration() {
  $items = array();
  $items['rules_swf_notified_when_new_startup_added'] = entity_import('rules_config', '{ "rules_swf_notified_when_new_startup_added" : {
      "LABEL" : "SWF notified when new Startup added",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "startup_groups" : "startup_groups" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "swfindlay1@gmail.com",
            "subject" : "New startup group added to Invrep",
            "message" : "Stephen,\\r\\n\\r\\n[node:author] has created a new startup group called: [node:title]\\r\\n\\r\\nThanks,\\r\\n - Team Invrep",
            "from" : "invrep@invrep.co",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Next steps for [node:title]",
            "message" : "[node:author],\\r\\n\\r\\nWe are pleased to confirm that [node:title] is now on Invrep.  Your Company can be accessed here: [node:url]\\r\\n\\r\\nWhat next:\\r\\n1. Add a Flash Update: for example, a major new contract win; or a new hire\\r\\n2. Add a Regular Report: click on \\u0022Latest Report\\u0022 tab and the \\u0022+ Add new Regular report\\u0022 on your Company page\\r\\n3. Invite investors to view your Company: click on \\u0022Your Followers\\u0022 tab on your Company page and then \\u0022Invite People\\u0022  \\r\\n\\r\\nYou can add or edit your background information, by clicking on the \\u0022Info\\u0022 tab and then \\u0022+ Edit your Company Information\\u0022 \\r\\n\\r\\nPlease see our Quick Start Guide: http:\\/\\/www.invrep.co\\/content\\/invrep-investor-reporting-platform-quick-start-guide \\r\\n\\r\\nPlease ask if you have any questions.\\r\\n\\r\\nEnjoy,\\r\\n - Team Invrep",
            "from" : "Invrep@invrep.co",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
