<?php
/**
 * @file
 * inv_financial_charts_and_tables_views_on_company_dashboard.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function inv_financial_charts_and_tables_views_on_company_dashboard_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_fuindraising|node|report|form';
  $field_group->group_name = 'group_fuindraising';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'report';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fundraising & Exit',
    'weight' => '42',
    'children' => array(
      0 => 'field_valuation',
      1 => 'field_valuation_basis',
      2 => 'field_exit',
      3 => 'field_funds_raised_report',
      4 => 'field_funds_targeted_rr',
      5 => 'field_fundraising_rr',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_fuindraising|node|report|form'] = $field_group;

  return $export;
}
