<?php
/**
 * @file
 * inv_financial_charts_and_tables_views_on_company_dashboard.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function inv_financial_charts_and_tables_views_on_company_dashboard_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'company_dashboard';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Company Dashboard';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'group_flash_updates_on_group',
      'display' => 'block',
      'args' => '',
      'title' => 'Flash Updates',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'group_quarterly_updates_on_group',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Latest Report',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'group_quarterly_updates_on_group',
      'display' => 'block_2',
      'args' => '',
      'title' => 'Financials',
      'weight' => '-97',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'quarterly_update_kpis',
      'display' => 'block_kpis',
      'args' => '',
      'title' => 'KPIs',
      'weight' => '-95',
      'type' => 'view',
    ),
    4 => array(
      'bid' => 'inv_boxes_delta_company_orbit_chart_kpis',
      'hide_title' => 1,
      'title' => 'Orbit Chart',
      'weight' => '-94',
      'type' => 'block',
    ),
    5 => array(
      'bid' => 'views_delta_8656cef8593dcc10def29591fa70fe5a',
      'hide_title' => 1,
      'title' => 'Cash',
      'weight' => '-93',
      'type' => 'block',
    ),
    6 => array(
      'bid' => 'views_delta_og_extras_members-block_1',
      'hide_title' => 1,
      'title' => 'Your Followers',
      'weight' => '-92',
      'type' => 'block',
    ),
    7 => array(
      'vid' => 'group_quarterly_updates_on_group',
      'display' => 'block',
      'args' => '',
      'title' => 'Old Reports',
      'weight' => '-92',
      'type' => 'view',
    ),
    8 => array(
      'vid' => 'group_fields_on_group',
      'display' => 'block',
      'args' => '',
      'title' => 'Info',
      'weight' => '-91',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'ui_tabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array(
    'history' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Cash');
  t('Company Dashboard');
  t('Financials');
  t('Flash Updates');
  t('Info');
  t('KPIs');
  t('Latest Report');
  t('Old Reports');
  t('Orbit Chart');
  t('Your Followers');

  $export['company_dashboard'] = $quicktabs;

  return $export;
}
