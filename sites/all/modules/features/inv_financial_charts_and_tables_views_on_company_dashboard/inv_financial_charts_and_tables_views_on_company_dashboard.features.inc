<?php
/**
 * @file
 * inv_financial_charts_and_tables_views_on_company_dashboard.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function inv_financial_charts_and_tables_views_on_company_dashboard_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function inv_financial_charts_and_tables_views_on_company_dashboard_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
