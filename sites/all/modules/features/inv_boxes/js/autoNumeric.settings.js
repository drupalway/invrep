(function ($) {
  'use strict'

  Drupal.behaviors.autoNumericReport = {
    attach: function (context) {

      var clearFields = function(fields) {
        $.each(fields, function(index, value) {
          var fieldSelector = '.'+ value + ' input';
          var fieldValue = $(fieldSelector, context).autoNumeric('get');
          $(fieldSelector, context).autoNumeric('destroy').val(fieldValue);
        });
      }

      var initAutoNumeric = function(fields, params) {
        $.each(fields, function(index, value) {
          $('.'+ value + ' input', context).autoNumeric(params);
        });
      }

      var numericFields = [
        'field-name-field-position-cash-in-bank',
        'field-name-field-budget-position-cash-in-ba',
        'field-name-field-monthly-cash-burn',
        'field-name-field-budget-monthly-cash-burn',
        'field-name-field-monthly-revenue',
        'field-name-field-budget-traction-monthly-re',
        'field-name-field-funds-targeted-rr',
        'field-name-field-funds-raised-report',
        'field-name-field-valuation'
      ];

      var percentageFields = [
        'field-name-field-kpi-gross-margin',
        'field-name-field-budget-kpi-gross-margin-pe',
        'field-name-field-ebit-margin-percentage',
        'field-name-field-budget-ebit-margin-percent'
      ];

      initAutoNumeric(numericFields, {vMin: '-100000000', vMax: '100000000', aDec: false});
      initAutoNumeric(percentageFields, {vMin: '0', vMax: '100', aDec: false});

      $('.node-report-form').on('submit', function(e) {
        clearFields(numericFields);
        clearFields(percentageFields);
      });
    }
  }

})(jQuery);