(function ($) {
  'use strict'

  Drupal.behaviors.verticalTabsAffix = {
    attach: function (context) {
      $('.vertical-tabs-list', context).once().affix({
        offset: {
          top: function () {
            return (this.top = $('.messages').outerHeight(true) + $('.boxes-box').outerHeight(true) + $('#branding').outerHeight(true))
          }
        }
      });
    }
  }

  Drupal.behaviors.verticalTabsAffixFix = {
    attach: function (context) {
      $('.vertical-tab-button a', context).click(function() {
        if ($(this).closest('.vertical-tabs-list').hasClass('affix')) {
          $('html, body').animate({scrollTop: $(".vertical-tabs").offset().top}, 0);
        }
      });
    }
  }
})(jQuery);