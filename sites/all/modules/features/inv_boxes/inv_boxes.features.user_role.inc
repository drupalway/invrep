<?php
/**
 * @file
 * inv_boxes.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function inv_boxes_user_default_roles() {
  $roles = array();

  // Exported role: Entrepreneur.
  $roles['Entrepreneur'] = array(
    'name' => 'Entrepreneur',
    'weight' => 5,
  );

  // Exported role: Investor.
  $roles['Investor'] = array(
    'name' => 'Investor',
    'weight' => 3,
  );

  return $roles;
}
