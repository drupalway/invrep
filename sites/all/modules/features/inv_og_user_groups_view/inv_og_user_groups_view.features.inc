<?php
/**
 * @file
 * inv_og_user_groups_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function inv_og_user_groups_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function inv_og_user_groups_view_image_default_styles() {
  $styles = array();

  // Exported image style: small_logo.
  $styles['small_logo'] = array(
    'name' => 'small_logo',
    'effects' => array(
      5 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 26,
          'height' => 26,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
