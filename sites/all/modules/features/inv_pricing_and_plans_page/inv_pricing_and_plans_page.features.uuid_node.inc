<?php
/**
 * @file
 * inv_pricing_and_plans_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function inv_pricing_and_plans_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Plans & Pricing v2',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '0fc17a58-03bf-44cd-83dc-f188aa44105b',
  'type' => 'page',
  'language' => 'und',
  'created' => 1380021493,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '6543560e-4462-4955-8236-7fee04a39469',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<?php
drupal_add_library(\'system\', \'ui.accordion\');
drupal_add_js(\'jQuery(document).ready(function(){jQuery("#accordion").accordion({collapsible: true, autoHeight: false, active: false});});\', \'inline\');
?>

<div class="planblock">
<div class="planheader">
<p class="grade">Premium</p>
<span>$ </span><span class="price">59</span><span> or £ </span><span class="price">49</span>
<p class="month">/month</p>
</div>
<div class="planstats">
<p>Great for <b>micro-VC</b> or <b>super-Angel</b> backed companies:</p>
<ul><li>Up to:</li>
<li><b>$1.5m / £1.0m</b> cash raised</li>
<li><b>$150k / £100k</b> revenue /mo</li></ul>

</div>
<div class="plandetails">


<div id="accordion">
<h3>Full feature list</h3>
<div>
<p>Your own Company Dashboard &bull; Unlimited Flash Updates &bull; Unlimited Quarterly Updates &bull; Unlimited Comments &bull; Unlimited administrators &bull; Invrep Reporting Templates &bull; Financial reporting templates &bull; Upload your own documents &bull; Reporting reminders &bull; Automated variance analysis &bull; Automated charting of results &bull; Unlimited Investors &bull; Unlimited investor alerts &bull; Manage your investor activity &bull; Backed up with Amazon &bull; Private and Secure &bull; <a href="http://www.invrep.co/content/elibrary">Resources and guidance</a> &bull; 25GB storage
</div>
</div>

<div class="middle"><a href="http://www.invrep.co/user/register"><button class="addcompany">Get started</button></a></div>
</div>
</div>

<p><b>Online HelpDesk: assistance with your reporting</b></p>

<p><b>Assistance with <a href="http://www.invrep.co/content/what-cap-table-and-how-prepare-one">cap table</a></b></p>

<p><b>Assistance with <a href="http://www.invrep.co/content/what-are-liquidation-curves-and-how-construct-them">liquidation curve</a></b></p>

<div class="planblock">
<div class="planheader">
<p class="grade">Pro</p>
<span>$ </span><span class="price">10</span><span> or £ </span><span class="price">10</span>
<p class="month">/month</p>
<p class="month">Up to 2 companies</p>
</div>
<div class="planstats">
<p>Dashboard analysis</p>
</div>
<div class="plandetails">

<b>Free 1 month trial</b>

<div class="middle"><a href="http://www.invrep.co/user/register"><button class="addcompany">Get started</button></a></div>
</div>

<div class="planblock">
<div class="planheader">
<p class="grade">Pro</p>
<span>$ </span><span class="price">10</span><span> or £ </span><span class="price">10</span>
<p class="month">/month</p>
<p class="month">Up to 2 companies</p>
</div>
<div class="planstats">
<p>Dashboard analysis</p>
</div>
<div class="plandetails">

<b>Free 1 month trial</b>

<div class="middle"><a href="http://www.invrep.co/user/register"><button class="addcompany">Get started</button></a></div>
</div>',
        'summary' => '',
        'format' => 'php_code',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-09-24 11:18:13 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Plans & Pricing v2',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'f80cdd9a-f9b2-4445-98ac-a0172a37bdf6',
  'type' => 'page',
  'language' => 'und',
  'created' => 1380021493,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '7383ce26-5ca1-4a3c-bc57-9cf146596079',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<?php
drupal_add_library(\'system\', \'ui.accordion\');
drupal_add_js(\'jQuery(document).ready(function(){jQuery("#accordion").accordion({collapsible: true, autoHeight: false, active: false});});\', \'inline\');
?>

<div class="planblock">
<div class="planheader">
<p class="grade">Premium</p>
<span>$ </span><span class="price">59</span><span> or £ </span><span class="price">49</span>
<p class="month">/month</p>
</div>
<div class="planstats">
<p>Great for <b>micro-VC</b> or <b>super-Angel</b> backed companies:</p>
<ul><li>Up to:</li>
<li><b>$1.5m / £1.0m</b> cash raised</li>
<li><b>$150k / £100k</b> revenue /mo</li></ul>

</div>
<div class="plandetails">


<div id="accordion">
<h3>Full feature list</h3>
<div>
<p>Your own Company Dashboard &bull; Unlimited Flash Updates &bull; Unlimited Quarterly Updates &bull; Unlimited Comments &bull; Unlimited administrators &bull; Invrep Reporting Templates &bull; Financial reporting templates &bull; Upload your own documents &bull; Reporting reminders &bull; Automated variance analysis &bull; Automated charting of results &bull; Unlimited Investors &bull; Unlimited investor alerts &bull; Manage your investor activity &bull; Backed up with Amazon &bull; Private and Secure &bull; <a href="http://www.invrep.co/content/elibrary">Resources and guidance</a> &bull; 25GB storage
</div>
</div>

<div class="middle"><a href="http://www.invrep.co/user/register"><button class="addcompany">Get started</button></a></div>
</div>
</div>

<p><b>Online HelpDesk: assistance with your reporting</b></p>

<p><b>Assistance with <a href="http://www.invrep.co/content/what-cap-table-and-how-prepare-one">cap table</a></b></p>

<p><b>Assistance with <a href="http://www.invrep.co/content/what-are-liquidation-curves-and-how-construct-them">liquidation curve</a></b></p>

<div class="planblock">
<div class="planheader">
<p class="grade">Pro</p>
<span>$ </span><span class="price">10</span><span> or £ </span><span class="price">10</span>
<p class="month">/month</p>
<p class="month">Up to 2 companies</p>
</div>
<div class="planstats">
<p>Dashboard analysis</p>
</div>
<div class="plandetails">

<b>Free 1 month trial</b>

<div class="middle"><a href="http://www.invrep.co/user/register"><button class="addcompany">Get started</button></a></div>
</div>

<div class="planblock">
<div class="planheader">
<p class="grade">Pro</p>
<span>$ </span><span class="price">10</span><span> or £ </span><span class="price">10</span>
<p class="month">/month</p>
<p class="month">Up to 2 companies</p>
</div>
<div class="planstats">
<p>Dashboard analysis</p>
</div>
<div class="plandetails">

<b>Free 1 month trial</b>

<div class="middle"><a href="http://www.invrep.co/user/register"><button class="addcompany">Get started</button></a></div>
</div>',
        'summary' => '',
        'format' => 'php_code',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-09-24 11:18:13 +0000',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Plans',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'fcf6290a-0197-4964-afa4-07e8c5a61c23',
  'type' => 'page',
  'language' => 'und',
  'created' => 1367515031,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '8bae7d56-9e9d-45c0-ab22-52b36011a1fc',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div style="border:1px solid #B8D0F5;background-color: #DDE9FB; padding:0.5em; margin-bottom:2em; text-align:center;color:#555;"><span></span><span style="font-size:1.8em;">2</span><span><b> months free</b> across all plans when you pay annually in advance</span></div>

<h3>Companies</h3>',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div style="border:1px solid #B8D0F5;background-color: #DDE9FB; padding:0.5em; margin-bottom:2em; text-align:center;color:#555;"><span></span><span style="font-size:1.8em;">2</span><span><b> months free</b> across all plans when you pay annually in advance</span></div>
<h3>Companies</h3>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'invrep',
  'picture' => 0,
  'data' => 'a:1:{s:7:"overlay";i:1;}',
  'date' => '2013-05-02 17:17:11 +0000',
);
  return $nodes;
}
