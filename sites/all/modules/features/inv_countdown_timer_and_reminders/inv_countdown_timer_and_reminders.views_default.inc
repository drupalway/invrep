<?php
/**
 * @file
 * inv_countdown_timer_and_reminders.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function inv_countdown_timer_and_reminders_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'countdown_timer_to_next_quarterly_report';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Countdown timer to next Quarterly report';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Countdown timer to next Quarterly report';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'og_context';
  $handler->display->display_options['access']['perm'] = 'update field_customer_qtr field';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'og_group_ref' => 'og_group_ref',
    'php' => 'php',
  );
  $handler->display->display_options['row_options']['separator'] = '|';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<ol style="text-align:left;margin-left:5%;"><b>Thank you for choosing Invrep!</b> Next steps:
<li>Invite investors and directors (<i>"Followers"</i>) to view your Company</li>
<li>Create a <i>Flash Update</i></li>
<li>Create your first <i>Regular Report</i></li></ol>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['empty']['area']['tokenize'] = TRUE;
  /* Relationship: OG membership: OG membership from Content */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  /* Relationship: OG membership: OG Roles from membership */
  $handler->display->display_options['relationships']['og_users_roles']['id'] = 'og_users_roles';
  $handler->display->display_options['relationships']['og_users_roles']['table'] = 'og_membership';
  $handler->display->display_options['relationships']['og_users_roles']['field'] = 'og_users_roles';
  $handler->display->display_options['relationships']['og_users_roles']['relationship'] = 'og_membership_rel';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Author uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'node';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  /* Field: Content: Period End Date */
  $handler->display->display_options['fields']['field_quarter_end_date']['id'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['field'] = 'field_quarter_end_date';
  $handler->display->display_options['fields']['field_quarter_end_date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_quarter_end_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Groups audience */
  $handler->display->display_options['fields']['og_group_ref']['id'] = 'og_group_ref';
  $handler->display->display_options['fields']['og_group_ref']['table'] = 'og_membership';
  $handler->display->display_options['fields']['og_group_ref']['field'] = 'og_group_ref';
  $handler->display->display_options['fields']['og_group_ref']['label'] = '';
  $handler->display->display_options['fields']['og_group_ref']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['og_group_ref']['settings'] = array(
    'link' => 1,
  );
  $handler->display->display_options['fields']['og_group_ref']['delta_offset'] = '0';
  /* Field: OG membership: Group_type */
  $handler->display->display_options['fields']['group_type']['id'] = 'group_type';
  $handler->display->display_options['fields']['group_type']['table'] = 'og_membership';
  $handler->display->display_options['fields']['group_type']['field'] = 'group_type';
  $handler->display->display_options['fields']['group_type']['exclude'] = TRUE;
  /* Field: OG membership: Group ID */
  $handler->display->display_options['fields']['gid']['id'] = 'gid';
  $handler->display->display_options['fields']['gid']['table'] = 'og_membership';
  $handler->display->display_options['fields']['gid']['field'] = 'gid';
  $handler->display->display_options['fields']['gid']['relationship'] = 'og_membership_rel';
  $handler->display->display_options['fields']['gid']['exclude'] = TRUE;
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = '';
  $handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
if(!isset($data->_field_data[\'nid\'][\'entity\']->field_quarter_end_date[\'und\'][0][\'value\'])){
  $data->_field_data[\'nid\'][\'entity\']->field_quarter_end_date[\'und\'][0][\'value\'] = "";
}

$qend = $data->_field_data[\'nid\'][\'entity\']->field_quarter_end_date[\'und\'][0][\'value\'];

if ($qend <> "")
  {
$now = time();
$expire = $qend + 90*24*60*60;
$remaining = ($now - $qend);
$minutesleft = ceil($remaining/60);
//extract days
$days = floor($minutesleft/(1440)); # Divide on the daily minutes 60 min * 24 hours
// extract hours
$hours = floor(($minutesleft-($days*1440))/60);
// extract left minutes
$minutes = ($minutesleft-($days*24*60)-($hours*60));
$due = -$days + 7;
$obj = NULL;
  }

if ($days < 50  && $days > 25)
    {
      $obj.="Is it time for a Monthly Report? ";
    }
elseif ($days >= 80)
    {
      $obj.="Is it time for a Quarterly Report? ";
    }
if ($days >= 0)
   {
        $obj .= $days . "d ";
    }    
if ($days >=0 && $hours > 0)
    {
        $obj .= $hours . "h ";
    }
    if ($days >=0 && $minutes >= 0)
    {
        $obj .= $minutes . "m ";
    }
   if($days >= 0)    
   {
   $obj .= "since your last Report";
   }  
print $obj;
if ($qend = "")
   {
   print "Is it time to create your first Regular Report?";
   }
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Sort criterion: Content: Period End Date (field_quarter_end_date) */
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['id'] = 'field_quarter_end_date_value';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['table'] = 'field_data_field_quarter_end_date';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['field'] = 'field_quarter_end_date_value';
  $handler->display->display_options['sorts']['field_quarter_end_date_value']['order'] = 'DESC';
  /* Contextual filter: User: Group membership (og_user_node) */
  $handler->display->display_options['arguments']['og_user_node_target_id']['id'] = 'og_user_node_target_id';
  $handler->display->display_options['arguments']['og_user_node_target_id']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['og_user_node_target_id']['field'] = 'og_user_node_target_id';
  $handler->display->display_options['arguments']['og_user_node_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['og_user_node_target_id']['default_argument_type'] = 'og_user_groups';
  $handler->display->display_options['arguments']['og_user_node_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['og_user_node_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['og_user_node_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['og_user_node_target_id']['break_phrase'] = TRUE;
  /* Contextual filter: Global: Null */
  $handler->display->display_options['arguments']['null']['id'] = 'null';
  $handler->display->display_options['arguments']['null']['table'] = 'views';
  $handler->display->display_options['arguments']['null']['field'] = 'null';
  $handler->display->display_options['arguments']['null']['default_action'] = 'default';
  $handler->display->display_options['arguments']['null']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'report' => 'report',
  );
  /* Filter criterion: Global: PHP */
  $handler->display->display_options['filters']['php']['id'] = 'php';
  $handler->display->display_options['filters']['php']['table'] = 'views';
  $handler->display->display_options['filters']['php']['field'] = 'php';
  $handler->display->display_options['filters']['php']['use_php_setup'] = 0;
  $handler->display->display_options['filters']['php']['php_filter'] = '$group_type = $row->group_type;
$gid = $row->gid;
if (empty($uid)) {
    global $user;
    $uid = $user->uid;}
$roles = og_get_user_roles ($group_type, $gid, $uid);
if (array_key_exists(\'3\', $roles) == FALSE || $roles[3] <> "administrator member")
  {return "TRUE";}';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['block_description'] = 'Countdown timer to next quarterly report';
  $export['countdown_timer_to_next_quarterly_report'] = $view;

  return $export;
}
