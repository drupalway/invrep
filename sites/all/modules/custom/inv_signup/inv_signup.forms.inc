<?php

/**
 * Implements hook_form_FORM_ID_alter().
 */
function inv_signup_form_user_register_form_alter(&$form, &$form_state) {
  //echo '<pre>'; print_r($form); echo '</pre>';
  $form['actions'] = array(
    '#type' => 'actions',
    'investor' => array(
      '#type' => 'submit',
      '#value' => t('Investor'),
    ),
    'entrepreneur' => array(
      '#type' => 'submit',
      '#value' => t('Entrepreneur'),
    ),
  );

  unset($form['buttons']);

  if (!empty($_GET['email'])) {
    $form['account']['mail']['#default_value'] = filter_xss($_GET['email']);
  }

  $form['#submit'] = array(
    'user_register_submit',
    'ctools_wizard_submit',
    'inv_signup_user_register_form_submit',
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function inv_signup_form_user_login_alter(&$form, &$form_state) {
  if (!empty($_GET['email'])) {
    $form['name']['#default_value'] = filter_xss($_GET['email']);
  }
}

/**
 * Investor / Entrepreneur selector for already registred user
 *
 * @param $form
 * @param $form_state
 */
function inv_user_registred_form($form, &$form_state) {
  $form['actions'] = array(
    '#type' => 'actions',
    'investor' => array(
      '#type' => 'submit',
      '#value' => t('Investor'),
    ),
    'entrepreneur' => array(
      '#type' => 'submit',
      '#value' => t('Entrepreneur'),
    ),
  );

  unset($form['buttons']);

  $form['#submit'] = array(
    'ctools_wizard_submit',
    'inv_signup_user_register_form_submit',
  );

  $form['#prefix'] = t('Are you...');

  //echo '<pre>'; print_r($form); echo '</pre>';

  return $form;
}

/**
 * Custom submit to set correct redirect after user creation investor or entrepreneur
 *
 * @param $form
 * @param $form_state
 */
function inv_signup_user_register_form_submit($form, &$form_state) {
  switch($form_state['clicked_button']['#id']) {
    case'edit-investor':
      if (!empty($GLOBALS['user']->uid)) {
        if ($role = user_role_load_by_name('Investor')) {
          user_multiple_role_edit(array($GLOBALS['user']->uid), 'add_role', $role->rid);
        }
      }
      $form_state['redirect'] = 'wizard/investor/invite';
      break;
    case'edit-entrepreneur':
      if (!empty($GLOBALS['user']->uid)) {
        if ($role = user_role_load_by_name('Entrepreneur')) {
          user_multiple_role_edit(array($GLOBALS['user']->uid), 'add_role', $role->rid);
        }
      }
      $form_state['redirect'] = 'wizard/entrepreneur/company';
      break;
  }
}

/**
 * General company info form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function inv_signup_entrepreneur_company_node_form($form, &$form_state) {
  module_load_include('inc', 'node', 'node.pages');
  form_load_include($form_state, 'inc', 'node', 'node.pages');

  //Initial node object
  global $user;
  $type = 'startup_groups';
  if (!empty($form_state['object']->company_nid)) {
    $node = node_load($form_state['object']->company_nid);
  }
  else {
    $node = (object) array(
      'uid' => $user->uid,
      'name' => (isset($user->name) ? $user->name : ''),
      'type' => $type,
      'language' => LANGUAGE_NONE,
    );
  }
  $form_state['build_info']['args'] = array($node);
  $form = drupal_retrieve_form($type . '_node_form', $form_state);

  $form['actions'] = array(); // remove default save (and preview) button

  $form['#prefix'] = t('<h2>Please add your company to get started</h2>');
  $form['body']['und'][0]['#wysiwyg'] = FALSE;

  $real_title = $form['title'];
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Company name',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
    '#autocomplete_path' => 'companies/autocomplete',
  );
  if (!empty($real_title['#default_value'])) {
    $form['title']['#default_value'] = $real_title['#default_value'];
  }

  //Comment out company name validation
  //$form['#validate'][] = 'custom_company_name_validate';

  //echo '<pre>'; print_r($form); echo '</pre>';

  return $form;
}

/**
 * Custom company name validator to check if company name is unique
 *
 * @param $form
 * @param $form_state
 */
function custom_company_name_validate($form, &$form_state) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'startup_groups');
  $query->propertyCondition('title', $form_state['values']['title']);
  $query->propertyCondition('uid', $GLOBALS['user']->uid, '!=');
  $flag = (bool)$query->range(0, 1)->count()->execute();
  if ($flag) {
    form_set_error('title', t('A company by this name already exists in the system'));
  }
}

/**
 * Notification and phone settings form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function inv_signup_entrepreneur_reminder_form($form, &$form_state) {
  module_load_include('inc', 'node', 'node.pages');
  form_load_include($form_state, 'inc', 'node', 'node.pages');

  $type = 'startup_groups';

  $company_arg_nid = arg(3);
  $is_existing_company = FALSE;
  if (is_numeric($company_arg_nid)) {
    $temp_node = node_load($company_arg_nid);
    if ($temp_node->type == 'startup_groups' && $temp_node->uid == $GLOBALS['user']->uid) {
      $form_state['object']->company_nid = $company_arg_nid;
      $is_existing_company = TRUE;
    }
  }
  $node = node_load($form_state['object']->company_nid);
  $form_state['build_info']['args'] = array($node);
  $form = drupal_retrieve_form($type . '_node_form', $form_state);

  $form['actions'] = array(); // remove default save (and preview) button
  $form['#groups']['group_reminders']->weight = -100;
  $form['phone_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Mobile phone'),
    '#description' => t('Please include international code: 001 (777) 7777777'),
    '#size' => 20,
    '#weight' => 1000,
  );
  $curr_user = user_load($GLOBALS['user']->uid);
  if (!empty($curr_user->field_phone['und'][0]['value'])) {
    $form['phone_number']['#default_value'] = $curr_user->field_phone['und'][0]['value'];
  }

  $form['#prefix'] = '<h2>' . t('<h2>We can send you reminders by email and SMS...') . '</h2>';
  $form['buttons']['next']['#value'] = t('Next');
  //echo '<pre>'; print_r($form); echo '</pre>';

  //Check invitation to notify investor about company creation
  if (!$is_existing_company) {
    $query = db_select('eck_invitation', 'ei');
    $query->fields('ei', array('uid', 'title'));
    $query->join('users', 'u', 'u.uid = ei.uid');
    $query->condition('u.status', 1, '=');
    $result = $query->condition('ei.title', $GLOBALS['user']->mail, '=')->execute();
    foreach ($result as $record) {
      $invited_by_user = user_load($record->uid);
      if (!empty($invited_by_user->mail)) {
        $invited_by_mail = $invited_by_user->mail;
        $parts = explode('@', $GLOBALS['user']->mail);
        $ceo_name = $parts[0];
        if (empty($ceo_name)) {
          $ceo_name = $GLOBALS['user']->name;
        }
        $params = array(
          '@ceo_name' => $ceo_name,
          '@company_name' => $node->title,
          '!company_url' => $GLOBALS['base_url'] . '/' . drupal_get_path_alias('node/' . $node->nid),
        );
        if (!isset($_SESSION['after_invitation'][$GLOBALS['user']->uid])) {
          $_SESSION['after_invitation'][$GLOBALS['user']->uid] = array();
        }
        if (!in_array($invited_by_mail, $_SESSION['after_invitation'][$GLOBALS['user']->uid])) {
          $values = array('state' => OG_STATE_ACTIVE);
          $field_name = og_get_best_group_audience_field('user', $invited_by_user->uid, 'node', 'startup_groups');
          if (!empty($field_name)) {
            $og_membership = og_membership_create('node', $node->nid, 'user', $invited_by_user->uid, $field_name, $values);
            $og_membership->save();
            drupal_mail('inv_signup', 'ceo_registred_after_invitation', $invited_by_mail, NULL, $params, variable_get('site_mail', NULL));
            $_SESSION['after_invitation'][$GLOBALS['user']->uid][] = $invited_by_mail;
          }
        }
      }
    }
  }

  return $form;
}

function inv_company_confirmation_form($form, &$form_state, $node) {
  //Do nothing
}

/**
 * Invitation Investors by Entrepreneur form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function inv_signup_entrepreneur_invite_form($form, &$form_state) {
  $form_state['build_info']['args'] = array('node', $form_state['object']->company_nid);
  $form = drupal_retrieve_form('_og_invite_people_new_users_form', $form_state);
  $form['#validate'][] = '_og_invite_people_new_users_form_validate';
  $form['actions'] = array(); // remove default save (and preview) button

  return $form;
}

/**
 * Investor CEOs invitation form
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function inv_signup_investor_invite_form($form, &$form_state) {
  $form['invite_ceos'] = array(
    '#type' => 'textfield',
    '#title' => t('CEO email(s)'),
    '#required' => FALSE,
    '#description' => t('Separate email addresses with comma, to invite multiple CEOs'),
  );

  $form['#prefix'] = '<h2>' . t('Invite your CEOs to start better reporting today...') . '</h2>';
  //echo '<pre>'; print_r($form); echo '</pre>';
  $form['buttons']['finish'] = $form['buttons']['return'];
  $form['buttons']['finish']['#value'] = t('Finish');
  $form['buttons']['return']['#value'] = t('Invite');

  return $form;
}

/**
 * inv_signup_investor_invite_form - Validate handler
 *
 * @param $form
 * @param $form_state
 */
function inv_signup_investor_invite_form_validate($form, &$form_state) {
  $invitee_emails = explode(',', preg_replace('/\s+/', '', $form_state['values']['invite_ceos']));
  foreach ($invitee_emails as $invitee_email) {
    if (!empty($invitee_email) && !valid_email_address($invitee_email)) {
      form_set_error('invitee', t('Invalid email(s).'));
    }
  }
}

/**
 * inv_signup_investor_invite_form - Submit handler
 *
 * @param $form
 * @param $form_state
 */
function inv_signup_investor_invite_form_submit($form, &$form_state) {
  $invitee_emails = explode(',', preg_replace('/\s+/', '', $form_state['values']['invite_ceos']));
  // Add group membership form.
  $values = array();

  foreach ($invitee_emails as $invitee_email) {
    if (!empty($invitee_email)) {
      $entity_type = 'invitation';
      $entity = entity_create('invitation', array('type' => 'invitation'));
      $wrapper = entity_metadata_wrapper($entity_type, $entity);
      $wrapper->title = $invitee_email;
      $wrapper->field_mail->set($invitee_email);
      $wrapper->uid = $GLOBALS['user']->uid;
      $wrapper->save();

      $message = '%mail has been invited to join Invrep';

      // Prepare parameters for message
      $parts = explode('@', $GLOBALS['user']->mail);
      $investor_name = $parts[0];
      if (empty($investor_name)) {
        $investor_name = $GLOBALS['user']->name;
      }
      $params = array(
        '@investor_name' => $investor_name,
        '!wizard_link' => $GLOBALS['base_url'] . url('wizard/signup/register', array('query' => array('email' => $invitee_email))),
      );

      drupal_set_message(t($message, array('%mail' => $invitee_email)));
      drupal_mail('inv_signup', 'ceos_invitation', $invitee_email, NULL, $params, variable_get('site_mail', NULL));
    }
  }
}

/**
 * Embedded views exposed form
 *
 * @param $form
 * @param $form_state
 * @return array|mixed
 */
function inv_signup_investor_search_form($form, &$form_state) {
  $view = views_get_view('og_extras_groups');
  $display_id = 'page';
  $view->set_display($display_id);
  $view->init_handlers(); // Initialize display handlers
  $form_state['view'] = $view;
  $form_state['display'] = $view->display_handler->display;
  $form_state['exposed_form_plugin'] = $view->display_handler->get_plugin('exposed_form'); // Exposed form plugins are used in Views 3
  $form_state['method'] = 'get';
  $form_state['rerender'] = TRUE;
  $form_state['no_redirect'] = TRUE;
  $form = drupal_build_form('views_exposed_form', $form_state); // Create the filter form

  //Move ctools trail block
  $form['title']['#prefix'] = $form['ctools_trail']['#markup'];
  $form['buttons']['return']['#value'] = t('Go');
  unset($form['ctools_trail'], $form['submit']);

  return $form;
}

/**
 * Submit handler to set correct step path and add cache variables
 *
 * @param $form
 * @param $form_state
 */
function inv_signup_form_submit($form, &$form_state) {
  if ($form_state['step'] == 'company') {
    //Save new company nid to cache object for using on the next step
    $form_state['object']->company_nid = $form_state['values']['nid'];
  }

  if ($form_state['step'] == 'reminder') {
    //Save new user phone number to cache object for using on the next step
    if (!empty($form_state['values']['phone_number'])) {
      $curr_user = user_load($GLOBALS['user']->uid);
      $curr_user->field_phone['und'][0]['value'] = filter_xss($form_state['values']['phone_number']);
      user_save($curr_user);
    }
  }

  $paths = ctools_wizard_get_path($form_state['form_info'], $form_state['clicked_button']['#next']);
  $form_state['redirect'] = $paths[0];
}
