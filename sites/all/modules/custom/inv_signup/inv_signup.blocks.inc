<?php

/**
 * Implements hook_block_info
 */
function inv_signup_block_info() {
  $blocks['invrep_homepage_signup'] = array(
    'info' => t('Custom Sign in/up multistep block'),
  );

  return $blocks;
}

/**
 * Implements hook__block_view
 */
function inv_signup_block_view($delta = '') {
  switch ($delta) {
    case 'invrep_homepage_signup':
      $block['subject'] = '';
      $block['content'] = invrep_homepage_signup();
      break;
  }

  return $block;
}

function invrep_homepage_signup() {
  $output = '';

  $form = drupal_get_form('homepage_signup_form');
  $output .= drupal_render($form);



  //TODO: create output for homepage block that contain Linkedin button and email feild
  return $output;
}

/**
 * Homepage register form block
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function homepage_signup_form($form, &$form_state) {
  if (user_access('use hybridauth') && user_is_anonymous()) {
    $form['hybridauth'] = array(
      '#type' => 'hybridauth_widget',
      '#title' => '',
      '#suffix' => '<div class="or-wrapper">OR</div>',
    );
  }

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('name@company.com'),
    '#required' => FALSE,
    '#default_value' => '',
    '#size' => 30,
    '#maxlength' => 40,
    '#weight' => 1000,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get started'),
    '#weight' => 1001,
    '#prefix' => '<div class="homepage-submit-wrapper">',
    '#suffix' => '<div class="btn-suffix-free">' . t('Free') . '</div></div>',
  );

  return $form;
}

/**
 * Validate handler for homepage_signup_form
 *
 * @param $form
 * @param $form_state
 */
function homepage_signup_form_validate($form, &$form_state) {
  $email = $form_state['values']['email'];
  if (empty($email) || !valid_email_address($email)) {
    form_set_error('email', t('Invalid email'));
  }
  else {
    $check_user = user_load_by_mail($email);
    if (!empty($check_user->uid) && $check_user->uid > 0) {
      drupal_goto('user', array('query' => array('email' => $email)));
    }
  }
}

/**
 * Submit handler for homepage_signup_form
 *
 * @param $form
 * @param $form_state
 */
function homepage_signup_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    url('wizard/signup/register'),
    array(
      'query' => array(
        'email' => $form_state['values']['email'],
      ),
    ),
  );
}
