<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<?php
/*
 * === CUSTOM CODE IN TEMPLATE SHOULD BE MOVED TO MODULE ===
 */

drupal_add_js('sites/all/libraries/highcharts/js/highcharts.js');
drupal_add_js('sites/all/libraries/highcharts/js/highcharts-more.js', array('type' => 'file', 'scope' => 'footer'));

$container = str_replace('_', '-', $variables['view']->name) . '-' . str_replace('_', '-', $variables['display_id']);

$series = array();
$companies_arr = array();
$n = 0;

$is_kpi_filter = FALSE;
$gid_filter = TRUE;
$og_list['node'] = array();
if ((!empty($variables['view']->args[1]) && $variables['view']->args[1] > 0) || (!empty($variables['view']->exposed_raw_input['kpi']) && $variables['view']->exposed_raw_input['kpi'] > 0)) {
  if (!empty($variables['view']->args[1]) && $variables['view']->args[1] > 0) {
    $kpi_filter = $variables['view']->args[1];
  }
  elseif (!empty($variables['view']->exposed_raw_input['kpi']) && $variables['view']->exposed_raw_input['kpi'] > 0) {
    $kpi_filter = $variables['view']->exposed_raw_input['kpi'];
    $gid_filter = FALSE;
    $og_list = og_get_groups_by_user($GLOBALS['user']);
  }
  $is_kpi_filter = TRUE;
  $kpi_terms = inv_company_handler_filter_qu_kpis_filter::get_limited_tids(FALSE, 'reports', $gid_filter, $og_list['node'], TRUE);
}
foreach ($variables['view']->result as $result_key => $result) {
  //If selected KPI term on dashboard
  if (!$is_kpi_filter
    || ($is_kpi_filter && in_array($result->nid, $kpi_terms[$kpi_filter]['reports']))) {
    if (!isset($companies_arr[$result->field_og_group_ref[0]['raw']['target_id']])) {
      $series[$n] = array('name' => render($result->field_og_group_ref[0]['rendered']));
      $companies_arr[$result->field_og_group_ref[0]['raw']['target_id']] = $n;
      $n++;
    }
    $current_n = $companies_arr[$result->field_og_group_ref[0]['raw']['target_id']];
    $series[$current_n]['date'] = render($result->field_field_quarter_end_date[0]['rendered']);

    $monthly_revenue_label = _get_field_label($variables['view']->field['field_monthly_revenue']);
    $cash_burn_label = _get_field_label($variables['view']->field['field_monthly_cash_burn']);
    $company_label = _get_field_label($variables['view']->field['og_group_ref']);
    $cash_in_bank_label = _get_field_label($variables['view']->field['field_position_cash_in_bank']);

    if (!$is_kpi_filter) {
      $x_val = intval(render($result->field_field_monthly_revenue[0]['rendered']));
    }
    elseif($is_kpi_filter > 0 && in_array($result->nid, $kpi_terms[$kpi_filter]['reports'])) {
      $x_val = intval($kpi_terms[$kpi_filter][$result->nid]['actual']);
    }

    $series[$current_n]['data'][] = array(
      'x' => $x_val,
      'y' => intval(render($result->field_field_monthly_cash_burn[0]['rendered'])),
      'z' => intval(render($result->field_field_position_cash_in_bank[0]['rendered'])),
      'date' => strip_tags(render($result->field_field_quarter_end_date[0]['rendered'])),
      'monthly_revenue' => $monthly_revenue_label . '<b>' . render($result->field_field_monthly_revenue[0]['rendered']) . '</b>',
      'cash_burn' => $cash_burn_label . '<b>' . render($result->field_field_monthly_cash_burn[0]['rendered']) . '</b>',
      'company' => $company_label . '<b>' . render($result->field_og_group_ref[0]['rendered']) . '</b>',
      'cash_in_bank' => $cash_in_bank_label . '<b>' . render($result->field_field_position_cash_in_bank[0]['rendered']) . '</b>',
    );
  }
}

$x_title = t('Traction: Monthly revenue');
if ($is_kpi_filter) {
  $kpi_term = taxonomy_term_load($kpi_filter);
  $x_title = $kpi_term->name;
}

$chart_arr = array(
  'chart' => array(
    'type' => 'bubble',
    'zoomType' => 'xy',
    'width' => null,
  ),
  'title' => array (
    'text' => '',
  ),
  'yAxis' => array(
    'title' => array(
      'text' => t('Profitability or (Cash burn)'),
    )
  ),
  'xAxis' => array(
    'title' => array(
      'text' => $x_title,
    )
  ),
  'plotOptions' => array(
    'bubble' => array(
      'tooltip' => array(
        'headerFormat' => '',
        'pointFormat' => '<b>{point.date}</b><br/>{point.monthly_revenue}<br/>{point.cash_burn}<br/>{point.cash_in_bank}<br/>{point.company}</br>',
      ),
    ),
  ),
  'series' => $series,
);

$json = json_encode($chart_arr);
//$highchart = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $json);
?>
<div class="<?php print $classes; ?>">
<?php print render($title_prefix); ?>
<?php if ($title): ?>
  <?php print $title; ?>
<?php endif; ?>
<?php print render($title_suffix); ?>
<?php if ($header): ?>
  <div class="view-header">
    <?php print $header; ?>
  </div>
<?php endif; ?>

<?php if ($exposed): ?>
  <div class="view-filters">
    <?php print $exposed; ?>
  </div>
<?php endif; ?>

<?php if ($attachment_before): ?>
  <div class="attachment attachment-before">
    <?php print $attachment_before; ?>
  </div>
<?php endif; ?>
<?php if (is_array($series) && count($series) > 0): ?>
  <div  class="charts-highchart chart" data-chart="<?php print htmlspecialchars($json); ?>" id="<?php print $container; ?>"></div>
<?php elseif (TRUE/*$empty*/): ?>
  <div class="view-empty">
    <?php print '<p>No Results Found</p>'; ?>
  </div>
<?php endif; ?>

<?php if ($pager): ?>
  <?php print $pager; ?>
<?php endif; ?>

<?php if ($attachment_after): ?>
  <div class="attachment attachment-after">
    <?php print $attachment_after; ?>
  </div>
<?php endif; ?>

<?php if ($more): ?>
  <?php print $more; ?>
<?php endif; ?>

<?php if ($footer): ?>
  <div class="view-footer">
    <?php print $footer; ?>
  </div>
<?php endif; ?>

<?php if ($feed_icon): ?>
  <div class="feed-icon">
    <?php print $feed_icon; ?>
  </div>
<?php endif; ?>

</div><?php /* class view */ ?>
