<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<?php
/*
 * === CUSTOM CODE IN TEMPLATE SHOULD BE MOVED TO MODULE ===
 */
drupal_add_js('sites/all/libraries/highcharts/js/highcharts.js');
drupal_add_js('sites/all/libraries/highcharts/js/highcharts-more.js', array('type' => 'file', 'scope' => 'footer'));

$container = str_replace('_', '-', $variables['view']->name) . '-' . str_replace('_', '-', $variables['display_id']);

$series = array();
$x_axis_categories = array();
//$companies_arr = array();
$n = 0;
//dsm($variables['view']->result);
$is_first = TRUE;

//Check company currency
$currency = _get_company_currency($variables['view']->result[0]->og_membership_gid);

$fields_settings_arr = array(
  'field_budget_traction_monthly_re' => array(
    'axis' => 0,
    'index' => 0,
    'color' => '#97B4D7',
    'type' => 'column',
    'suffix' => ' ' . $currency,
  ),
  'field_monthly_revenue' => array(
    'axis' => 0,
    'index' => 1,
    'color' => '#386192',
    'type' => 'column',
    'suffix' => ' ' . $currency,
  ),
  'field_budget_monthly_cash_burn' => array(
    'axis' => 0,
    'index' => 2,
    'color' => '#d8b2d8',
    'type' => 'column',
    'suffix' => ' ' . $currency,
  ),
  'field_monthly_cash_burn' => array(
    'axis' => 0,
    'index' => 3,
    'color' => '#663399',
    'type' => 'column',
    'suffix' => ' ' . $currency,
  ),
  'field_kpi_gross_margin' => array(
    'axis' => 1,
    'index' => 4,
    'color' => '#BBBBBB',
    'type' => 'spline',
    'suffix' => '%',
    'marker' => array('symbol' => 'circle'),
  ),
  'field_budget_kpi_gross_margin_pe' => array(
    'axis' => 1,
    'index' => 5,
    'color' => '#BBBBBB',
    'type' => 'spline',
    'suffix' => '%',
    'dashStyle' => 'dash',
    'marker' => array('symbol' => 'circle'),
  ),
  'field_ebit_margin_percentage' => array(
    'axis' => 1,
    'index' => 6,
    'color' => '#498356',
    'type' => 'spline',
    'suffix' => '%',
    'marker' => array('symbol' => 'diamond'),
  ),
  'field_budget_ebit_margin_percent' => array(
    'axis' => 1,
    'index' => 7,
    'color' => '#498356',
    'type' => 'spline',
    'suffix' => '%',
    'dashStyle' => 'dash',
    'marker' => array('symbol' => 'diamond'),
  ),
);

foreach ($variables['view']->result as $result_key => $result) {
  $x_axis_categories[] = strip_tags(render($result->field_field_quarter_end_date[0]['rendered']));
  if ($is_first) {
    foreach ($fields_settings_arr as $field => $settings) {
      $obj_field_name = 'field_' . $field;
      $data = (!empty($result->{$obj_field_name}[0]['rendered'])) ? intval(render($result->{$obj_field_name}[0]['rendered'])) : null;
      $series[$settings['index']] = array(
        'name' => _get_field_label($variables['view']->field[$field], FALSE),
        'color' => $settings['color'],
        'type' => $settings['type'],
        'data' => array($data),
        'tooltip' => array(
          'valueSuffix' => $settings['suffix'],
        ),
      );
      if (!empty($settings['axis'])) {
        $series[$settings['index']]['yAxis'] = $settings['axis'];
      }
      if (!empty($settings['dashStyle'])) {
        $series[$settings['index']]['dashStyle'] = $settings['dashStyle'];
      }
      if (!empty($settings['marker'])) {
        $series[$settings['index']]['marker'] = $settings['marker'];
      }
    }
    $is_first = FALSE;
  }
  else {
    foreach ($fields_settings_arr as $field => $settings) {
      $obj_field_name = 'field_' . $field;
      $series[$settings['index']]['data'][] = (!empty($result->{$obj_field_name}[0]['rendered'])) ? intval(render($result->{$obj_field_name}[0]['rendered'])) : null;
    }
  }
}

$chart_arr = array(
  'chart' => array(
    'zoomType' => 'xy',
  ),
  'title' => array (
    'text' => '',
  ),
  /*'legend' => array(
    'width' => '100%',
  ),*/
  'xAxis' => array(
    'categories' => $x_axis_categories,
  ),
  'yAxis' => array(
    array(
      'title' => array(
        'text' => 'Revenue, Cash Burn' . ((!empty($currency)) ? ' (' . $currency .')' : ''),
        'style' => array(
          'color' => '#89A54E',
        ),
      ),
    ),
    array(
      'title' => array(
        'text' => 'Gross margin, EBIT margin (%)',
        'style' => array(
          'color' => '#4572A7',
        ),
      ),
      'labels' => array (
        'format' => '{value}%',
      ),
      'opposite' => true,
    )
  ),
  'series' => $series,
);
if (count($variables['view']->result) > 0) {
  $json = json_encode($chart_arr);
}
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($json)): ?>

    <div  class="charts-highchart chart" data-chart="<?php print htmlspecialchars($json); ?>" id="<?php print $container; ?>"></div>

  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
