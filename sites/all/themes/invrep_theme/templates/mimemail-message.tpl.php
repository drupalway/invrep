<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>>
    <div id="center">
      <div id="main">
        <table class="mail-table" width="100%" align="center" cellpadding="0" cellspacing="0">
          <tr style="background-color: #F7F7F7;">
            <td width="15%" style="border-bottom: 1px solid #D4D4CC;"> </td>
            <td class="mail-header" width="70%" style="padding: 10px 0 15px 0; border-bottom: 1px solid #D4D4CC;">
              <a href="http://invrep.co"><img src="http://www.invrep.co/sites/default/files/styles/adaptive_swf/adaptive-image/public/Invrep_Grey.png" border="0"/></a>
            </td>
            <td width="15%" style="border-bottom: 1px solid #D4D4CC;"> </td>
          <tr>
          <tr class="mail-content">
            <td width="15%"> </td>
            <td width="70%" style="font-size: 14px; padding: 20px 0;">
              <?php print $body ?>
            </td>
            <td width="15%"> </td>
          </tr>
          <tr class="mail-footer" style="background-color: #464646;">
            <td width="15%" style="border-top: 2px dashed #fff;"> </td>
            <td width="70%" style="border-top: 2px dashed #fff; padding: 10px 0; color: #FFF;">
              <div class="content clearfix">
                <div id="boxes-box-footer_links" class="boxes-box"><div class="boxes-box-content"><p style="clear:left;margin-bottom:5px;"><u>Resources</u></p>
                    <ul style="margin-bottom: 20px; list-style: disc outside none; padding-left: 0;">
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/invrep-investor-reporting-platform-quick-start-guide">Quick start guide</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/faqs">FAQs</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/resources-for-investor-reporting-board-governance-startups-SMEs">eLibrary • Resources</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/plans">Pricing</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/principles">Principles</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none;  float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/is-your-startup-worth-1m">Quiz: is your startup worth $1m?</a></li>
                    </ul>
                    <p style="clear:left;margin-bottom:5px;"><br><u>About</u></p>
                    <ul style="list-style: disc outside none; padding-left: 0;">
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/join-team">We're hiring!</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/about-us">Team</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/press">Press &amp; Blog</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/contact-us">Contact</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/terms-and-conditions">Terms &amp; Conditions</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/platform">Investor reporting platform</a></li>
                      <li style="margin-left: 0; list-style: none outside none;"><a style="color: #FFFFFF; text-decoration: none; border-right: 1px solid #FFFFFF; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/elibrary">Reporting Resources</a></li>
                      <li style="margin-left: 0; list-style: none outside none;" class="last"><a style="color: #FFFFFF; text-decoration: none; float: left; font-size: 11px; line-height: 1.1em; margin-bottom: 5px; margin-right: 1.5%; padding-right: 1.5%;" href="http://www.invrep.co/content/principles">Board reporting principles</a></li>
                    </ul>
                    <p style="clear: left;"><img class="adaptive-image" style="float:right; clear:right;" src="http://invrep.co/sites/default/files/styles/adaptive_swf/adaptive-image/public/logo_white.png"></p>
                    <p style="float:right; clear:right;">© Invrep.co 2013</p>
                    <p><br></p>
                    <p style="float:left;"><i>in·vrep : 1 (verb) Assisting CEOs to deliver world-class investor reporting for startups and SMEs.  2 (noun) Better connected entrepreneurs and shareholders<br>
                        Invrep is created from best-of-breed portfolio-management strategies used by leading VCs and private equity firms. Companies on Invrep benefit from this knowledge and expertise, to deliver world class investor reporting in their start-ups and SMEs.  Invrep is suitable as an investor reporting platform for angel backed companies; VC backed companies and crowdfunded companies; as well as new startups, accelerators and incubators</i></p>
                  </div>
                </div>
              </div>
            </td>
            <td width="15%" style="border-top: 2px dashed #fff;"> </td>
          </tr>
        </table>
      </div>
    </div>
  </body>
</html>
