
(function ($) {

  Drupal.behaviors.custom = {
    attach: function (context, settings) {
      //Do nothing for the moment
    }
  };

  // Signup behaviour
  Drupal.behaviors.signup = {
    attach: function (context, settings) {
      $('.homepage-submit-wrapper').click(function () {
        $('#homepage-signup-form').submit();
      });

      $('label[for=edit-field-revenue-model-und]').click(function () {
        $('#edit-field-revenue-model-und').slideToggle(500);
      });
      $('label[for=edit-field-target-customers-und]').click(function () {
        $('#edit-field-target-customers-und').slideToggle(500);
      });

      //Move Phone field
      $('.page-wizard-entrepreneur-reminder #inv-signup-entrepreneur-reminder-form .form-item-phone-number').appendTo('#edit-group_reminders .fieldset-wrapper');
      $('#edit-group_reminders .fieldset-wrapper .fieldset-wrapper .form-item-phone-number').remove();

      //Add description on company autocomplete
      $('.page-wizard #inv-signup-entrepreneur-company-node-form #edit-title').focusin(function () {
        var tid = setInterval(check_autocomplete, 2000);
        function check_autocomplete() {
          if ($('.page-wizard #inv-signup-entrepreneur-company-node-form .form-item-title #autocomplete ul li').length > 0 && !$('.page-wizard #inv-signup-entrepreneur-company-node-form #edit-title').hasClass('throbbing')) {
            $('.page-wizard #inv-signup-entrepreneur-company-node-form .form-item-title .form-required').html('*&nbsp;&nbsp;&nbsp;This company already exists. Join to confirm you are a team member, or create a new company.');
          }
          else {
            $('.page-wizard #inv-signup-entrepreneur-company-node-form .form-item-title .form-required').html('*');
          }
        }
      });

      $('.page-wizard #inv-signup-entrepreneur-company-node-form #edit-title').focusout(function () {
        $('.page-wizard #inv-signup-entrepreneur-company-node-form .form-item-title .form-required').html('*');
        clearInterval(tid);
      });

    }
  };

}(jQuery));