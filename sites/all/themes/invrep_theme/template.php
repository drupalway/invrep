<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
 
function omega_invrep_theme_css_alter(&$css) {
    foreach ($css as $key => $value) {
        if (preg_match('/^ie::(\S*)/', $key)) {
            unset($css[$key]); 
        } else {
            $css[$key]['browsers']['IE'] = TRUE;
        }
    } 
}

function invrep_theme_preprocess_html(&$vars) {
  drupal_add_css(drupal_get_path('theme', 'invrep_theme') . '/css/ie.css', array(
    'group' => 2000,
    'preprocess' => FALSE,
    'browsers' => array('IE' => 'IE', '!IE' => TRUE),
    'weight' => 100
  ));
}

